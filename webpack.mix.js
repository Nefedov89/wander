const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Client.
mix.js('resources/assets/client/js/app.js', 'public/assets/client/js')
  .sass('resources/assets/client/sass/app.scss', 'public/assets/client/css')
  .copyDirectory('resources/assets/client/images', 'public/assets/client/images');

// Admin.
mix.copyDirectory('resources/assets/acp/css', 'public/assets/acp/css')
  .copyDirectory('resources/assets/acp/images', 'public/assets/acp/images')
  .copyDirectory('resources/assets/acp/locales', 'public/assets/acp/locales')
  .copyDirectory('resources/assets/acp/js/main', 'public/assets/acp/js/main');

mix.js([
  'resources/assets/acp/js/theme.js',
  'resources/assets/acp/js/custom.js',
  'resources/assets/acp/js/app.js',
],
'public/assets/acp/js/main.js'
);

// Common.
mix.js([
  'resources/assets/common/js/utils/notify.js',
  'resources/assets/common/js/utils/confirmation.js',
],
'public/assets/common/js/main.js'
);
