<?php

declare(strict_types = 1);

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use App\Models\User;

/**
 * Class UsersSeeder
 */
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->getUsers() as $data) {
            $user = User::query()->updateOrCreate(
                ['email' => $data['email']],
                Arr::except($data, ['role'])
            );

            if ($data['role']) {
                $role = Role::query()
                    ->where('name', $data['role'])
                    ->first();

                if ($role) {
                    $user->assignRole($role);
                }
            }

            $user->markEmailAsVerified();
        }
    }

    /**
     * @return array
     */
    protected function getUsers(): array
    {
        $pass = bcrypt('pass');

        return [
            [
                'name'     => 'Sergey Nefedov',
                'email'    => 'nefedovs89@gmail.com',
                'role'     => 'admin',
                'password' => $pass,
            ],
            [
                'name'     => 'John Doe',
                'email'    => 'john_doe@test.com',
                'role'     => null,
                'password' => $pass,
            ],
        ];
    }
}
