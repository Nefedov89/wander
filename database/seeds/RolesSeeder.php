<?php

declare(strict_types = 1);

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

/**
 * Class RolesSeeder
 */
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Role::query()->updateOrCreate(['name' => 'admin']);
    }
}
