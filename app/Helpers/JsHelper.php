<?php

declare(strict_types = 1);

namespace App\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use const null;

use function array_merge_recursive, array_replace_recursive, json_encode;

/**
 * Class JsHelper
 *
 * @package App\Helpers
 */
class JsHelper
{
    /**
     * @var array
     */
    protected static $config = [];

    /**
     * @return array
     */
    public static function get(): array
    {
        return self::$config;
    }

    /**
     * @param array $config
     *
     * @return void
     */
    public static function set(array $config): void
    {
        self::$config = $config;
    }

    /**
     * @param array $data
     *
     * @return void
     */
    public static function push(array $data): void
    {
        self::$config = array_replace_recursive(self::$config, $data);
    }

    /**
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public static function pull(string $key, $default = null)
    {
        return Arr::pull(self::$config, $key, $default);
    }

    /**
     * @return string
     */
    public static function toString(): string
    {
        return json_encode(
            array_merge_recursive(self::defaultConfigs(), self::$config)
        );
    }

    /**
     * @return array
     */
    protected static function defaultConfigs(): array
    {
        $user = Auth::user();

        $data = [
            'user' => [
                'id'    => $user ? $user->getKey() : null,
                'email' => $user ? $user->getAttribute('email') : null,
                'name'  => $user ? $user->getAttribute('name') : null,
            ],
        ];

        if (Session::has('notification')) {
            $data['notification'] = Session::get('notification');
        }

        return $data;
    }
}
