<?php

declare(strict_types = 1);

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyEmailNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Hash;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App\Models
 */
class User extends Authenticatable implements HasMedia, JWTSubject, MustVerifyEmail
{
    use Notifiable, HasMediaTrait, HasRoles;

    public const MEDIA_COLLECTION_AVATAR = 'avatar';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'language',
        'email_verification_token',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'name'                     => 'string',
        'email'                    => 'string',
        'password'                 => 'string',
        'language'                 => 'string',
        'email_verification_token' => 'string',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'avatar_url',
        'tours_count',
        'is_admin',
    ];

    /**
     * Entity relations go below.
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tours(): HasMany
    {
        return $this->hasMany(
            Tour::class,
            'user_id',
            $this->primaryKey
        );
    }

    /**
     * Entity scopes go below.
     */

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmins(Builder $query): Builder
    {
        return $query->whereHas('roles', static function (Builder $sQuery) {
            $sQuery->where('name', 'admin');
        });
    }

    /**
     * Entity mutators and accessors go below.
     */

    /**
     * @return string
     */
    public function getAvatarUrlAttribute(): string
    {
        $url = $this->getFirstMediaUrl(static::MEDIA_COLLECTION_AVATAR);

        return $url !== ''
            ? $url
            : asset('assets/client/images/profile/profile-user.png');
    }

    /**
     * @return int
     */
    public function getToursCountAttribute(): int
    {
        return $this->tours()->count();
    }

    /**
     * @return bool
     */
    public function getIsAdminAttribute(): bool
    {
        return $this->hasRole('admin');
    }
    /**
     * @return string
     */
    public function getMainImageUrlAttribute(): string
    {
        $image = $this->getFirstMedia(static::MEDIA_COLLECTION_AVATAR);

        return $image !== null
            ? $image->getFullUrl()
            : asset('assets/client/images/profile/profile-user.png');
    }

    /**
     * Entity public methods go below.
     */

    /**
     * Defines if all media should be preserved when deleting a model.
     *
     * @return boolean
     */
    public function shouldDeletePreservingMedia(): bool
    {
        return false;
    }

    /**
     * @return array
     */
    public function allowedAvatarMimeTypes(): array
    {
        return [
            'image/jpeg',
            'image/png',
        ];
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Send the password reset notification.
     *
     * @param mixed $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification());
    }
}
