<?php

declare(strict_types = 1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use const false;

/**
 * Class Tour
 *
 * @package App\Models
 */
class Tour extends Model implements HasMedia
{
    use HasMediaTrait;

    public const MODELS_PER_PAGE = 5;

    public const PLACE_PHOTO_MEDIA_COLLECTION = 'place_photo';

    /**
     * @var array
     */
    protected $fillable = [
        'city_name',
        'city_id',
        'date',
        'time_start',
        'time_end',
        'transportation_type',
        'start_point',
        'places',
        'user_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'city_name'           => 'string',
        'city_id'             => 'string',
        'date'                => 'date',
        'time_start'          => 'datetime',
        'time_end'            => 'datetime',
        'transportation_type' => 'string',
        'start_point'         => 'array',
        'places'              => 'array',
        'user_id'             => 'int',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'places_photos',
    ];

    /**
     * Entity relations go below.
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(
            User::class,
            'user_id',
            'id',
            'user'
        );
    }

    /**
     * Entity scopes go below.
     */

    /**
     * Entity mutators and accessors go below.
     */

    /**
     * @return string
     */
    public function getDateAttribute(): string
    {
        return (new Carbon($this->attributes['time_start']))
                     ->format('m/d/Y');
    }

    /**
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    public function getTimeStartAttribute(): string
    {
        return Carbon::createFromTimeString($this->attributes['time_start'])
                     ->format('H:i');
    }

    /**
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    public function getTimeEndAttribute(): string
    {
        return Carbon::createFromTimeString($this->attributes['time_end'])
                     ->format('H:i');
    }

    /**
     * @return array
     */
    public function getPlacesPhotosAttribute(): array
    {
        $photos = [];

        foreach ($this->getMedia(static::PLACE_PHOTO_MEDIA_COLLECTION) as $photo) {
            $photos[$photo->getCustomProperty('place_id')] = $photo->getFullUrl();
        }

        return $photos;
    }

    /**
     * @return bool
     */
    public function shouldDeletePreservingMedia(): bool
    {
        return false;
    }
}
