<?php

declare(strict_types = 1);

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class ResetPasswordNotification
 *
 * @package Illuminate\Auth\Notifications
 */
class ResetPasswordNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a notification instance.
     *
     * @param string $token
     *
     * @return void
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param mixed $notifiable
     *
     * @return array|string
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        $actionUrl = url(
            sprintf(
                '%s/reset-password/%s?email=%s',
                config('app.url'),
                $this->token,
                $notifiable->getAttribute('email')
            )
        );

        return (new MailMessage)
            ->subject(Lang::get('auth.reset_password_email.subject'))
            ->line(Lang::get('auth.reset_password_email.line_1'))
            ->action(
                Lang::get('auth.reset_password_email.action'),
                $actionUrl
            )
            ->line(Lang::get('auth.reset_password_email.line_2'));
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param mixed $callback
     *
     * @return void
     */
    public static function toMailUsing($callback): void
    {
        static::$toMailCallback = $callback;
    }
}
