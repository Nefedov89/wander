<?php

declare(strict_types = 1);

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

/**
 * Class VerifyEmailNotification
 *
 * @package App\Notifications
 */
class VerifyEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var \Closure|null
     */
    public static $toMailCallback;


    /**
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }

        return (new MailMessage)
            ->subject(Lang::get('auth.verify_email.subject'))
            ->line(Lang::get('auth.verify_email.call_to_action'))
            ->action(
                Lang::get('auth.verify_email.btn'),
                $this->verificationUrl($notifiable)
            )
            ->line(Lang::get('auth.verify_email.tip'));
    }

    /**
     * @param \Closure $callback
     *
     * @return void
     */
    public static function toMailUsing(\Closure $callback): void
    {
        static::$toMailCallback = $callback;
    }

    /**
     * @param mixed $notifiable
     *
     * @return string
     */
    protected function verificationUrl($notifiable): string
    {
        return URL::temporarySignedRoute(
            'auth.verification.verify',
            Carbon::now()->addMinutes(60),
            [
                'id'    => $notifiable->getKey(),
                'token' => $notifiable->getAttribute('email_verification_token'),
                'email' => $notifiable->getAttribute('email'),
            ]
        );
    }
}
