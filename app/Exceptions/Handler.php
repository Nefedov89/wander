<?php

declare(strict_types = 1);

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Throwable;

/**
 * Class Handler
 *
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    protected const MAX_HTTP_CODE = 599;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     *
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception): void
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param mixed      $request
     * @param \Exception $exception
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if ($request->wantsJson() && !$this->isAcpRequest($request)) {
            return Response::json(
                $this->getJsonErrorResponseData($exception),
                $this->getExceptionCode($exception)
            );
        }

        if ($exception instanceof UnauthorizedException && $this->isAcpRequest($request)) {
            return redirect()->route('acp.auth.login');
        }

        return parent::render($request, $exception);
    }

    /**
     * @param Exception $exception
     *
     * @return array
     */
    protected function getJsonErrorResponseData(Exception $exception): array
    {
        return [
            'code'  => $this->getExceptionCode($exception),
            'error' => $this->getExceptionMessage($exception),
        ];
    }

    /**
     * @param Throwable $e
     *
     * @return int
     */
    protected function getExceptionCode(Throwable $e): int
    {
        if (is_callable([$e, 'getStatusCode'])) {
            $code = $e->getStatusCode();
        } else {
            $code = $e->getCode();
        }

        if ($e instanceof ValidationException) {
            $code = $e->status;
        }

        return $code >= HttpResponse::HTTP_BAD_REQUEST && $code <= static::MAX_HTTP_CODE
            ? (int) $code
            : HttpResponse::HTTP_INTERNAL_SERVER_ERROR;
    }

    /**
     * @param Throwable $e
     *
     * @return string
     */
    protected function getExceptionMessage(Throwable $e): string
    {
        $message = $e->getMessage();

        if (Config::get('app.debug')) {
            $message .= "\nFile: ".$e->getFile().':'.$e->getLine();
        }

        return $message;
    }

    /**
     * @param mixed $request
     *
     * @return bool
     */
    private function isAcpRequest($request): bool
    {
        return $request->segment(1) === 'acp';
    }
}
