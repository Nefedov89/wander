<?php

declare(strict_types = 1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

/**
 * Class ResolveLang
 *
 * @package App\Http\Middleware
 */
class ResolveLang
{
    /**
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $language = $user ? $user->getAttribute('language') : Config::get('app.fallback_locale');

        App::setLocale($language);

        return $next($request);
    }
}
