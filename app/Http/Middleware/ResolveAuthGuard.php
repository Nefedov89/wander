<?php

declare(strict_types = 1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class ResolveAuthGuard
 *
 * @package App\Http\Middleware
 */
class ResolveAuthGuard
{
    /**
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->segment(1) === 'acp') {
            Auth::shouldUse('web');
        }

        return $next($request);
    }
}
