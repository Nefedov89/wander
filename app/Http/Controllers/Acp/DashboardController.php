<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

/**
 * Class DashboardController
 *
 * @package App\Http\Controllers\Acp
 */
class DashboardController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function index(): RedirectResponse
    {
        return Redirect::route('acp.user.index');
    }
}
