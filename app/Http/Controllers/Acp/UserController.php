<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Helpers\JsHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Acp\User\StoreRequest;
use App\Http\Requests\Acp\User\UpdateRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Role;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Acp
 */
class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        JsHelper::push(
            [
                'user' => [
                    'roles' => Role::query()
                        ->select(['id as value', 'name as label'])
                        ->get()
                        ->toArray(),
                ],
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index(): ViewContract
    {
        return View::make('acp.user.index');
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function create(): ViewContract
    {
        return View::make('acp.user.create');
    }

    /**
     * @param \App\Http\Requests\Acp\User\StoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $data = $request->only(['name', 'email']);

        if ($password = $request->get('password')) {
            $data['password'] = $password;
        }

        $user = User::create($data);

        Event::dispatch(new Registered($user));

        $user->roles()->sync(
            Arr::pluck($request->get('roles'), 'value')
        );

        Session::flash(
            'success',
            Lang::get('acp/user.messages.create')
        );

        return $this->noContentResponse();
    }

    /**
     * @param \App\Models\User $user
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(User $user): ViewContract
    {
        return View::make(
            'acp.user.edit',
            [
                'user' => $user,
            ]
        );
    }

    /**
     * @param \App\Http\Requests\Acp\User\UpdateRequest $request
     *
     * @param \App\Models\User                          $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, User $user): JsonResponse
    {
        $data = $request->only(['name', 'email']);

        if ($password = $request->get('password')) {
            $data['password'] = $password;
        }

        $user->update($data);

        $user->roles()->sync(
            Arr::pluck($request->get('roles'), 'value')
        );

        Session::flash(
            'success',
            Lang::get('acp/user.messages.update')
        );

        return $this->noContentResponse();
    }

    /**
     * @param \App\Models\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function delete(User $user): JsonResponse
    {
        $user->delete();

        Session::flash(
            'success',
            Lang::get('acp/user.messages.delete')
        );

        return $this->noContentResponse();
    }

    /**
     * @param \App\Models\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function activate(User $user): JsonResponse
    {
        $user->setAttribute('is_active', true);
        $user->save();

        Session::flash(
            'success',
            Lang::get('acp/user.messages.activate')
        );

        return $this->noContentResponse();
    }

    /**
     * @param \App\Models\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function deactivate(User $user): JsonResponse
    {
        $user->setAttribute('is_active', false);
        $user->save();

        Session::flash(
            'success',
            Lang::get('acp/user.messages.deactivate')
        );

        return $this->noContentResponse();
    }

    /**
     * @param \App\Models\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(User $user): JsonResponse
    {
        return $this->successResponse($user->toArray());
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $query = $request->get('query');

        $data = User::query()
            ->where(
                'name',
                'like',
                "%{$query}%"
            )
            ->orWhere(
                'email',
                'like',
                "%{$query}%"
            )
            ->select([
                'id as value',
                'name',
                'email',
                DB::raw('concat(name, ": ", email) as label'),
            ])
            ->orderBy('name')
            ->get()
            ->toArray();

        return $this->successResponse($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \InvalidArgumentException
     */
    public function getAll(Request $request): JsonResponse
    {
        $users = User::query()
            ->when(
                $request->get('name'),
                static function ($query, $name) {
                    $query->where('name', 'like', "%{$name}%");
                }
            )
            ->when(
                $request->get('email'),
                static function ($query, $email) {
                    $query->where('email', 'like', "%{$email}%");
                }
            )
            ->when(
                $request->get('id'),
                static function ($query, $id) {
                    $query->where('id', 'like', "{$id}%");
                }
            )
            ->latest()
            ->paginate(20)
            ->toArray();

        return $this->successResponse($users);
    }
}
