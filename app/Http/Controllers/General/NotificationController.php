<?php

declare(strict_types = 1);

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use function strtolower;

/**
 * Class NotificationController
 *
 * @package App\Http\Controllers\General
 */
class NotificationController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUnreadNotifications(): JsonResponse
    {
        return $this->successResponse(
            Auth::user()->getAttribute('unreadNotifications')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $trainings = Auth::user()
            ->notifications()
            ->when(
                $request->get('search'),
                static function ($query, $search) {
                    $query->whereRaw(
                        'LOWER(data->"$.message") like ?',
                        '%'.strtolower($search).'%'
                    );
                }
            )
            ->latest()
            ->paginate(10);

        return $this->successResponse($trainings);
    }

    /**
     * @param \Illuminate\Notifications\DatabaseNotification $databaseNotification
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(DatabaseNotification $databaseNotification): JsonResponse
    {
        if (!(Auth::user())->getAttribute('notifications')->contains($databaseNotification)) {
            return $this->json()->badRequest();
        }

        $databaseNotification->markAsRead();

        return $this->successResponse([
            'notification' => $databaseNotification->fresh(),
            'message'      => Lang::get('notifications.messages.marked_as_read'),
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function readAll(): JsonResponse
    {
        $user = Auth::user();
        $user->getAttribute('unreadNotifications')->markAsRead();

        return $this->successResponse([
            'notifications' => $user->getAttribute('notifications'),
            'message'       => Lang::get('notifications.messages.mark_all_as_read'),
        ]);
    }
}
