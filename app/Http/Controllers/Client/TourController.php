<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tour\StoreRequest;
use App\Models\Tour;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

use function array_merge;

/**
 * Class TourController
 *
 * @package App\Http\Controllers\Client
 */
class TourController extends Controller
{
    /**
     * @param \App\Http\Requests\Tour\StoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \InvalidArgumentException
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $dataFromRequest = $request->all();
        $dataFromRequest['places'] = array_map(
            static function ($place) {
                unset($place['photo']);

                return $place;
            },
            $dataFromRequest['places']
        );

        $data = array_merge(['user_id' => Auth::id()], $dataFromRequest);

        $dateFormatMaps = [
            'date'       => 'Y-m-d',
            'time_start' => 'H:i',
            'time_end'   => 'H:i',
        ];

        foreach ($dateFormatMaps as $field => $format) {
            if (isset($data[$field])) {
                $data[$field] = Carbon::createFromFormat($format, $data[$field]);
            }
        }

        $tour = Tour::query()->create($data);

        foreach ($request->get('places') as $place) {
            $tour->addMediaFromUrl($place['photo'] ?? URL::asset('/images/home/bg.png'))
                ->withCustomProperties(['place_id' => $place['id']])
                ->usingFileName($place['id'])
                ->toMediaCollection(Tour::PLACE_PHOTO_MEDIA_COLLECTION);
        }

        return $this->entityCreatedResponse();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \InvalidArgumentException
     */
    public function index(Request $request): JsonResponse
    {
        return $this->successResponse(
            Tour::query()->where('user_id', Auth::id())
                ->latest()
                ->paginate(Tour::MODELS_PER_PAGE)
                ->toArray()
        );
    }

    /**
     * @param \App\Models\Tour $tour
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Tour $tour): JsonResponse
    {
        return $this->successResponse([
            'tour' => $tour,
        ]);
    }

    /**
     * @param \App\Models\Tour $tour
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function delete(Tour $tour): JsonResponse
    {
        if ($tour->getAttribute('user_id') !== Auth::id()) {
            return $this->badRequestResponse([
                'message' => Lang::get('components/tour.messages.not_yours'),
            ]);
        }

        $tour->delete();

        return $this->successResponse([]);
    }
}
