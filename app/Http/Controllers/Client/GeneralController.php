<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\General\ContactUsRequest;
use App\Models\User;
use App\Notifications\ContactUsNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;

/**
 * Class GeneralController
 *
 * @package App\Http\Controllers\Client
 */
class GeneralController extends Controller
{
    /**
     * @param \App\Http\Requests\General\ContactUsRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function contactUs(ContactUsRequest $request): JsonResponse
    {
        Notification::send(
            User::query()->admins()->get(),
            new ContactUsNotification($request->all())
        );

        return $this->successResponse([]);
    }
}
