<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\AddRequest;
use App\Models\User;
use App\Notifications\AddedUserNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Client
 */
class UserController extends Controller
{
    /**
     * @param AddRequest $request
     *
     * @return JsonResponse
     */
    public function addUser(AddRequest $request): JsonResponse
    {
        if (!Auth::user()->hasRole('acp')) {
            return $this->badRequestResponse([
                'message' => Lang::get('components/add_user.messages.not_allowed')
            ]);
        }

        $password = Str::random(8);

        $user = User::query()->create([
            'name'     => $request->get('email'),
            'email'    => $request->get('email'),
            'password' => Hash::make($password),
            'language' => 'ru',
        ]);
        $user->markEmailAsVerified();

        $user->notify(new AddedUserNotification($password));

        return $this->entityCreatedResponse();
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function changeLanguage(Request $request): JsonResponse
    {
        $user = Auth::user();
        $user->update(['language' => $request->get('language')]);

        return $this->successResponse(['user' => $user->fresh()]);
    }
}
