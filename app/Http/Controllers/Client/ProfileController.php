<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use const null;
use function bcrypt, array_filter;

/**
 * Class ProfileController
 *
 * @package App\Http\Controllers\Client
 */
class ProfileController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(): JsonResponse
    {
        return $this->successResponse([
            'user'        => Auth::user(),
            // todo: set created tours count.
            'toursNumber' => 11,
        ]);
    }

    /**
     * @param \App\Http\Requests\ProfileRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProfileRequest $request): JsonResponse
    {
        $user = Auth::user();

        $data = array_filter($request->all());

        if (isset($data['password']) && $data['password'] !== null) {
            $data['password'] = bcrypt($data['password']);
        }

        $user->update($data);

        if (isset($data['avatar']) && $data['avatar'] instanceof UploadedFile) {
            $user->clearMediaCollection(User::MEDIA_COLLECTION_AVATAR);
            $user->addMedia($data['avatar'])
                 ->toMediaCollection(User::MEDIA_COLLECTION_AVATAR);
        }

        return $this->successResponse([
            'user' => $user->fresh(),
        ]);
    }
}
