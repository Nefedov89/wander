<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

/**
 * Class VerificationController
 *
 * @package App\Http\Controllers\Client\Auth
 */
class VerificationController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify(Request $request): RedirectResponse
    {
        /** @var User $user */
        $user = User::query()
            ->where([
                'id' => $request->route('id'),
                'email_verification_token' => $request->route('token'),
            ])
            ->first();

        if (! $user) {
            return Redirect::to('/')
               ->with(
                   'warning',
                   Lang::get('auth.verify_email.not_verified')
               );
        }

        $user->markEmailAsVerified();

        event(new Verified($user));

        return Redirect::to('/login?email='.$request->get('email'))
           ->with(
               'success',
               Lang::get('auth.verify_email.verified')
           );
    }
}
