<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use const false;
use function time;

/**
 * Class LoginController
 *
 * @package App\Http\Controllers\Client\Auth
 */
class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @param  \App\Http\Requests\Auth\LoginRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (false !== ($token = $this->attemptLogin($request))) {
            return $this->sendLoginResponse($request, $token);
        }

        $this->incrementLoginAttempts($request);

        return $this->entityUnprocessableResponse([
            'message' => Lang::get('auth.failed'),
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): JsonResponse
    {
        $this->guard()->logout();

        return $this->noContentResponse();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken(): JsonResponse
    {
        return $this->successResponse([
            'token'            => $this->guard()->refresh(),
            'user'             => $this->guard()->user(),
            'token_expires_at' => time() + $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendLockoutResponse(Request $request): JsonResponse
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return $this->tooManyRequestsResponse(
            ['message' => Lang::get('auth.throttle', ['seconds' => $seconds])]
        );
    }

    /**
     * @param  Request $request
     *
     * @return bool|string
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt($this->credentials($request));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param mixed                    $token
     *
     * @return JsonResponse
     */
    protected function sendLoginResponse(
        Request $request,
        $token
    ): JsonResponse {
        $this->clearLoginAttempts($request);

        return $this->authenticated($this->guard()->user(), $token);
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string                                     $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function authenticated(
        Authenticatable $user,
        string $token
    ): JsonResponse {
        if (!$user->hasVerifiedEmail()) {
            return $this->unauthorizedResponse(
                ['message' => Lang::get('auth.not_confirmed')]
            );
        }

        return $this->successResponse([
            'token'            => $token,
            'user'             => $user,
            'token_expires_at' => time() + $this->guard()->factory()->getTTL() * 60
        ]);
    }
}
