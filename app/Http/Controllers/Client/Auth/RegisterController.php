<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Client\Auth;

use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\VerifyEmailNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

/**
 * Class RegisterController
 *
 * @package App\Http\Controllers\Client\Auth
 */
class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * @param \App\Http\Requests\Auth\RegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $user = $this->create($request->all());
        $user->notify(new VerifyEmailNotification);

        return $this->successResponse();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\Models\User
     */
    protected function create(array $data): User
    {
        return User::create([
            'name'                     => $data['name'],
            'email'                    => $data['email'],
            'email_verification_token' => Str::random(8),
            'password'                 => Hash::make($data['password']),
        ]);
    }
}
