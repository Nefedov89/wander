<?php

declare(strict_types = 1);

namespace App\Http\Controlles\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use function array_merge;

/**
 * Trait JsonResponseTrait
 *
 * @package App\Http\Controlles\Traits
 */
trait JsonResponseTrait
{
    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function entityCreatedResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge($data, ['code' => HttpResponse::HTTP_CREATED]),
            HttpResponse::HTTP_CREATED,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function entityUnprocessableResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge(
                $data,
                ['code' => HttpResponse::HTTP_UNPROCESSABLE_ENTITY]
            ),
            HttpResponse::HTTP_UNPROCESSABLE_ENTITY,
            $headers,
            $options
        );
    }

    /**
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function noContentResponse(
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            ['code' => HttpResponse::HTTP_NO_CONTENT],
            HttpResponse::HTTP_NO_CONTENT,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function notFoundResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge($data, ['code' => HttpResponse::HTTP_NOT_FOUND]),
            HttpResponse::HTTP_NOT_FOUND,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function methodNotAllowedResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge(
                $data,
                ['code' => HttpResponse::HTTP_METHOD_NOT_ALLOWED]
            ),
            HttpResponse::HTTP_METHOD_NOT_ALLOWED,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function badRequestResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge($data, ['code' => HttpResponse::HTTP_BAD_REQUEST]),
            HttpResponse::HTTP_BAD_REQUEST,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function successResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            $data,
            HttpResponse::HTTP_OK,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function unauthorizedResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge($data, ['code' => HttpResponse::HTTP_UNAUTHORIZED]),
            HttpResponse::HTTP_UNAUTHORIZED,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    public function tooManyRequestsResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge(
                $data,
                ['code' => HttpResponse::HTTP_TOO_MANY_REQUESTS]
            ),
            HttpResponse::HTTP_TOO_MANY_REQUESTS,
            $headers,
            $options
        );
    }

    /**
     * @param array $data
     * @param array $headers
     * @param int   $options
     *
     * @return JsonResponse
     */
    protected function internalErrorResponse(
        array $data = [],
        array $headers = [],
        int $options = 0
    ): JsonResponse {
        return Response::json(
            array_merge(
                $data,
                ['code' => HttpResponse::HTTP_INTERNAL_SERVER_ERROR]
            ),
            HttpResponse::HTTP_INTERNAL_SERVER_ERROR,
            $headers,
            $options
        );
    }
}
