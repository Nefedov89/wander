<?php

declare(strict_types = 1);

namespace App\Http\Requests\General;

use App\Http\Requests\AppFormRequest;
use Illuminate\Support\Facades\App;

/**
 * Class ContactUsRequest
 *
 * @package App\Http\Requests\General
 */
class ContactUsRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        App::setLocale($this->request->get('lang'));

        return [
            'email'   => 'required|email',
            'name'    => 'required|string',
            'message' => 'required|string',
        ];
    }
}
