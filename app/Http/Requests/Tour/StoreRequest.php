<?php

declare(strict_types = 1);

namespace App\Http\Requests\Tour;

use App\Http\Requests\AppFormRequest;

/**
 * Class StoreRequest
 *
 * @package App\Http\Requests
 */
class StoreRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'city_id'             => ['required', 'string'],
            'city_name'           => ['required', 'string'],
            'date'                => ['required', 'date'],
            'time_start'          => ['required', 'date_format:H:i'],
            'time_end'            => ['required', 'date_format:H:i', 'after:time_start'],
            'transportation_type' => ['required', 'string'],
            'start_point'         => ['required', 'array'],
            'places'              => ['required', 'array'],
            'places.*'            => ['required', 'array'],
            'places.*.id'         => ['required', 'string'],
            'places.*.name'       => ['required', 'string'],
            'places.*.position'   => ['required', 'numeric'],
        ];
    }
}
