<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 *
 * @package App\Http\Requests\Acp\User
 */
class UpdateRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email',
                'max:255',
                "unique:users,email,{$this->request->get('id')}",
            ],
            'password' => [
                'nullable',
                'min:6',
                'confirmed',
            ],
            'avatar' => [
                'nullable',
                'image',
            ],
            'roles' => [
                'nullable',
                'array',
            ],
            'roles.*.value' => [
                'nullable',
                'numeric',
                'exists:roles,id',
            ],
        ];
    }
}
