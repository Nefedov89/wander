<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 *
 * @package App\Http\Requests\Acp\User
 */
class StoreRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email',
                'max:255',
                "unique:users,email",
            ],
            'password' => [
                'required',
                'min:6',
                'confirmed',
            ],
            'avatar' => [
                'nullable',
                'image',
            ],
            'roles' => [
                'nullable',
                'array',
            ],
            'roles.*.value' => [
                'nullable',
                'numeric',
                'exists:roles,id',
            ],
        ];
    }
}
