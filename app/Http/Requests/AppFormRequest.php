<?php

declare(strict_types = 1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use const PHP_EOL;
use function array_filter, array_keys, implode;

/**
 * Class AppFormRequest
 *
 * @package App\Http\Requests
 */
class AppFormRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function getDataFromRules(): array
    {
        return $this->all(array_keys($this->container->call([$this, 'rules'])));
    }

    /**
     * @return array
     */
    public function getFilledDataFromRules(): array
    {
        return array_filter($this->getDataFromRules());
    }

    /**
     * @param Validator $validator
     *
     * @return void
     *
     * @throws HttpResponseException
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator): void
    {
        if ($this->wantsJson()) {
            throw new HttpResponseException(
                new JsonResponse(
                    [
                        'errors' => $validator->errors(),
                        'code'   => HttpResponse::HTTP_UNPROCESSABLE_ENTITY,
                    ],
                    HttpResponse::HTTP_UNPROCESSABLE_ENTITY
                )
            );
        }

        parent::failedValidation($validator);
    }
}
