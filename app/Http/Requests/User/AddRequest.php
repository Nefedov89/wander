<?php

declare(strict_types = 1);

namespace App\Http\Requests\User;

use App\Http\Requests\AppFormRequest;

/**
 * Class AddRequest
 *
 * @package App\Http\Requests\User
 */
class AddRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                'max:255',
                'unique:users,email',
            ],
        ];
    }
}
