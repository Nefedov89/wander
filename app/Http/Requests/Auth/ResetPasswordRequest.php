<?php

declare(strict_types = 1);

namespace App\Http\Requests\Auth;

use App\Http\Requests\AppFormRequest;

/**
 * Class ResetPasswordRequest
 *
 * @package App\Http\Requests\Auth
 */
class ResetPasswordRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }
}
