<?php

declare(strict_types = 1);

namespace App\Http\Requests\Auth;

use App\Http\Requests\AppFormRequest;

/**
 * Class LoginRequest
 *
 * @package App\Http\Requests\Auth
 */
class LoginRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => 'required|email|string|max:255',
            'password' => 'required|string',
        ];
    }
}
