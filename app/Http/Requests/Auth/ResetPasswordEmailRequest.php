<?php

declare(strict_types = 1);

namespace App\Http\Requests\Auth;

use App\Http\Requests\AppFormRequest;

/**
 * Class ResetPasswordEmailRequest
 *
 * @package App\Http\Requests\Auth
 */
class ResetPasswordEmailRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|email|exists:users,email',
        ];
    }
}
