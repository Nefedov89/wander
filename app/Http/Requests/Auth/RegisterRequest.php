<?php

declare(strict_types = 1);

namespace App\Http\Requests\Auth;

use App\Http\Requests\AppFormRequest;

/**
 * Class RegisterRequest
 *
 * @package App\Http\Requests\Auth
 */
class RegisterRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'                 => 'required|string|max:255',
            'email'                => 'required|string|email|max:255|unique:users',
            'password'             => 'required|string|min:6|confirmed',
            'terms_and_conditions' => 'required|accepted',
        ];
    }
}
