<?php

declare(strict_types = 1);

namespace App\Http\Requests;

/**
 * Class ProfileRequest
 *
 * @package App\Http\Requests
 */
class ProfileRequest extends AppFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'     => 'required|string|max:255',
            'email'    => 'required|email|string|unique:users,email,'.$this->request->get('id'),
            'password' => 'nullable|string|min:6|confirmed',
            'avatar'   => 'nullable|image',
        ];
    }
}
