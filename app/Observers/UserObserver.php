<?php

declare(strict_types = 1);

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Str;

/**
 * Class UserObserver
 *
 * @package App\Observers
 */
class UserObserver
{
    /**
     * @param \App\Models\User $user
     *
     * @return void
     */
    public function created(User $user): void
    {
        $user->update([
            'email_verification_token' => Str::random(8),
        ]);
    }
}
