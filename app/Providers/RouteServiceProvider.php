<?php

declare(strict_types = 1);

namespace App\Providers;

use App\Models\Tour;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Class RouteServiceProvider
 *
 * @package App\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->mapPatterns();
        $this->mapModels();

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
    }

    /**
     * @return void
     */
    protected function mapWebRoutes(): void
    {
        Route::namespace($this->namespace)
            ->group($this->app->basePath().'/routes/web.php');
    }

    /**
     * @return void
     */
    protected function mapPatterns(): void
    {
        Route::pattern('tour', '[0-9]+');
        Route::pattern('user', '[0-9]+');
    }

    /**
     * @return void
     */
    protected function mapModels(): void
    {
        Route::model('tour', Tour::class);
        Route::model('user', User::class);
    }
}
