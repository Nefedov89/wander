@if (Session::has('error'))
    <div id="errorAlert" style="display: none">{{ Session::get('error') }}</div>
    <script>
        notify.error(document.getElementById('errorAlert').innerText, '');
    </script>
@endif

@if (Session::has('info'))
    <div id="infoAlert" style="display: none">{{ Session::get('info') }}</div>
    <script>
        notify.info(document.getElementById('infoAlert').innerText, '');
    </script>
@endif

@if (Session::has('warning'))
    <div id="warningAlert" style="display: none">{{ Session::get('warning') }}</div>
    <script>
        notify.warning(document.getElementById('warningAlert').innerText, '');
    </script>
@endif

@if (Session::has('success'))
    <div id="successAlert" style="display: none">{{ Session::get('success') }}</div>
    <script>
        notify.success(document.getElementById('successAlert').innerText, '');
    </script>
@endif

