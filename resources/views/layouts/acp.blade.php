<!DOCTYPE html>
<html lang="{{ Lang::locale() }}">
    <head>
        <title>@lang('acp/common.title')</title>
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{{ URL::asset('assets/client/images/header_logo.svg') }}" type="image/x-icon">
        <link rel="stylesheet" href="{{ URL::asset('assets/acp/css/icons/icomoon/styles.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/acp/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/acp/css/bootstrap_limitless.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/acp/css/layout.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/acp/css/components.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/acp/css/colors.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/acp/css/custom.css') }}">
        @yield('styles')
    </head>
    <body class="navbar-top">
        <div id="app">
            <!-- Main navbar -->
            @yield('navbar', View::make('acp.partial.navbar'))
            <!-- /main navbar -->
            <!-- Page container -->
            <div class="@yield('container-class')">
                <!-- Page content -->
                <div class="page-content">
                    <!-- Main sidebar -->
                    @yield('sidebar', View::make('acp.partial.sidebar'))
                    <!-- /main sidebar -->
                    <!-- Main content -->
                    <div class="content-wrapper">
                        <!-- Page header -->
                        @yield('header', View::make('acp.partial.header'))
                        <!-- /page header -->
                        <!-- Content area -->
                        <div class="content @yield('content-class')">
                            @include('acp.partial.alerts')
                            @yield('content')
                        </div>
                        <!-- /content area -->
                        <!-- Footer -->
                        @include('acp.partial.footer')
                        <!-- /footer -->
                    </div>
                    <!-- /main content -->
                </div>
                <!-- /page content -->
            </div>
            <!-- /page container -->
        </div>
        <!-- Scripts -->
        <script>
            window.wander = {!! App\Helpers\JsHelper::toString() !!};
        </script>
        <script src="{{ URL::asset('assets/acp/js/main/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('assets/acp/js/main/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ URL::asset('assets/common/js/laroute.js') }}"></script>
        <script src="{{ URL::asset('assets/common/js/main.js') }}"></script>
        <script src="{{ URL::asset('assets/acp/js/main.js') }}"></script>
        @yield('scripts')
    </body>
</html>
