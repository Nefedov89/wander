@extends('layouts.acp')

@section('navbar')
@endsection

@section('sidebar')
@endsection

@section('header')
@endsection

@section('content-class')d-flex justify-content-center align-items-center @endsection

@section('content')
    <!-- Simple login form -->
    <form method="POST" action="{{ route('acp.auth.login.check') }}" class="login-form">
        {{ csrf_field() }}

        <div class="card mb-0">
            <div class="card-body">
                <div class="text-center mb-3">
                    <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                    <h5 class="mb-0">@lang('acp/common.login.title')</h5>
                    <span class="d-block text-muted">@lang('acp/common.login.subtitle')</span>
                </div>
                <div class="form-group form-group-feedback form-group-feedback-left">
                    <input
                        id="email"
                        type="email"
                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        name="email"
                        value="{{ old('email') }}"
                        required autofocus
                        placeholder="@lang('acp/common.login.email')"
                    >
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <div class="form-control-feedback">
                        <i class="icon-user text-muted"></i>
                    </div>
                </div>
                <div class="form-group form-group-feedback form-group-feedback-left">
                    <input
                        id="password"
                        type="password"
                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                        name="password"
                        required
                        placeholder="@lang('acp/common.login.password')"
                    >
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <div class="form-control-feedback">
                        <i class="icon-lock2 text-muted"></i>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        @lang('acp/common.login.sign_in') <i class="icon-circle-right2 ml-2"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
    <!-- /simple login form -->
@endsection
