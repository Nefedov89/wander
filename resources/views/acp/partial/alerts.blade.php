@php
    $close = $close ?? true;
@endphp

@includeWhen(Session::has('success'), 'acp.partial.alerts.success', [
    'close'     => $close,
    'alertText' => Session::get('success')
])

@includeWhen(Session::has('info'), 'acp.partial.alerts.info', [
    'close'     => $close,
    'alertText' => Session::get('info')
])

@includeWhen(Session::has('warning'), 'acp.partial.alerts.warning', [
    'close'     => $close,
    'alertText' => Session::get('warning')
])

@includeWhen(Session::has('error'), 'acp.partial.alerts.danger', [
    'close'     => $close,
    'alertText' => Session::get('error')
])
