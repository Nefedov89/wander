<div class="navbar navbar-expand-md navbar-dark navbar-slide-top fixed-top">
    <div class="navbar-brand">
        <a href="/" class="d-inline-block">
            <img class="logo-acp" src="{{ URL::asset('assets/acp/images/logo-white.svg') }}" alt="" style="min-height: 25px">
        </a>
    </div>
    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>
        <span class="badge bg-success ml-md-3 mr-md-auto">online</span>
        {{--<notifications-badge></notifications-badge>--}}
        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    @php $user = Auth::user(); @endphp
                    <img src="{{ $user->getAttribute('avatar_url') }}" class="rounded-circle mr-2" height="34" alt="">
                    <span>{{ $user->getAttribute('name') }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a
                        class="dropdown-item"
                        href="#"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                    >
                        <i class="icon-switch2"></i>
                        @lang('acp/common.navbar.logout')
                    </a>

                    <form id="logout-form" action="{{ route('acp.auth.logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>
