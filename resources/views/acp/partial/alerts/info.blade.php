<div class="alert alert-absolute alert-info alert-white">
    @if(!empty($close))
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
            <span class="sr-only">@lang('general.word.close')</span>
        </button>
    @endif
    <span class="icon">
        <i class="fa fa-info-circle"></i>
    </span>
    @if(isset($alertTitle))
        <span class="text-semibold">
            {{ $alertTitle }}
        </span>
    @endif
    @if(is_array($alertText))
        <ul>
            @foreach($alertText as $message)
                <li>{!! $message !!}</li>
            @endforeach
        </ul>
    @else
        {!! $alertText !!}
    @endif
</div>
