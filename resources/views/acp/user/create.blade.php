@extends('layouts.acp')

@section('page_title')
    <i class="icon-add"></i>
    @lang('acp/user.create.title')
@endsection

@section('breadcrumbs')
    <a class="breadcrumb-item" href="@route('acp.user.index')">
        @lang('acp/user.breadcrumbs.index')
    </a>
    <span class="breadcrumb-item active">
        @lang('acp/user.breadcrumbs.create')
    </span>
@endsection

@section('content')
    <user-create></user-create>
@endsection
