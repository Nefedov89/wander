@extends('layouts.acp')

@section('page_title')
    <i class="icon-pencil"></i>
    @lang('acp/user.edit.title', ['name' => $user->getAttribute('name')])
@endsection

@section('breadcrumbs')
    <a class="breadcrumb-item" href="@route('acp.user.index')">
        @lang('acp/user.breadcrumbs.index')
    </a>
    <span class="breadcrumb-item active">
        @lang('acp/user.breadcrumbs.edit')
    </span>
@endsection

@section('content')
    <user-edit :user-id="{{ $user->getKey() }}"></user-edit>
@endsection
