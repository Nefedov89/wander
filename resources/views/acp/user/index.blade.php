@extends('layouts.acp')

@section('page_title')
    <i class="icon-list"></i>
    @lang('acp/user.index.title')
@endsection

@section('breadcrumbs')
    <span class="breadcrumb-item active">
        @lang('acp/user.breadcrumbs.index')
    </span>
@endsection

@section('search')
    {{--<search-model--}}
        {{--placeholder="@lang('acp/user.search')"--}}
        {{--path-to-model="@route('acp.user.edit', ':id')"--}}
        {{--search-path="@route('acp.user.search')"--}}
    {{--></search-model>--}}
@endsection

@section('content')
    <user-index></user-index>
@endsection
