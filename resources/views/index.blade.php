<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scaleable=no, minimal-ui">
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <meta name="theme-color" content="#009ada">
        <title>Wander</title>
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet" defer>
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/client/css/app.css') }}">
    </head>
    <body>
        <div id="app">
            <v-app>
                <layout-app-header></layout-app-header>
                <notification-message></notification-message>
                <transition name="slide-left" mode="out-in">
                    <v-content>
                        <router-view></router-view>
                    </v-content>
                </transition>
                <layout-app-footer></layout-app-footer>
            </v-app>
        </div>
        <!-- Scripts -->
        <script>
            window.wander = {!! App\Helpers\JsHelper::toString() !!};
        </script>
        <script src="{{ asset('assets/common/js/laroute.js') }}"></script>
        <script src="{{ asset('assets/client/js/app.js') }}"></script>
        <!-- Hotjar Tracking Code for wander.cool -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1987321,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
        <!-- Alerts -->
        @include('layouts.partial.alerts')
    </body>
</html>
