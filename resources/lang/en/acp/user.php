<?php

declare(strict_types = 1);

return [
    'search' => 'Search users by name or email',
    'breadcrumbs' => [
        'index' => 'Users',
        'create' => 'Create user',
        'edit' => 'Edit user',
    ],
    'form' => [
        'title' => 'General information',
        'name' => 'Name',
        'email' => 'Email',
        'phone' => 'Phone',
        'password' => 'Password',
        'password_confirmation' => 'Password confirmation',
        'roles' => 'Roles',
    ],
    'index' => [
        'title' => 'Users',
        'header_btn' => 'Create new user',
        'filters' => [
            'title' => 'Users filters',
            'name' => 'Filter by name',
            'email' => 'Filter by email',
            'id' => 'Filter by ID',
        ],
        'table' => [
            'headers' => [
                'id' => 'ID',
                'name' => 'Name',
                'email' => 'Email',
                'role' => 'Role',
                'verified' => 'Verified',
                'is_active' => 'Is Active',
                'lang' => 'Language',
                'created_at' => 'Created at',
            ],
            'body' => [
                'verified' => 'Verified',
                'not_verified' => 'Not verified',
            ],
        ],
    ],
    'create' => [
        'title' => 'Create user',
    ],
    'edit' => [
        'title' => 'Edit user <u>:name</u>',
    ],
    'messages' => [
        'create' => 'User has been successfully created',
        'update' => 'User has been successfully updated',
        'delete' => 'User has been successfully deleted',
        'activate' => 'User has been successfully activated',
        'deactivate' => 'User has been successfully deactivated',
    ],
];
