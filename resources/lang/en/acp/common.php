<?php

declare(strict_types = 1);

return [
    'title'   => 'Admin',
    'navbar'  => [
        'profile' => 'Profile',
        'logout'  => 'Logout',
    ],
    'sidebar' => [
        'users' => 'Users',
    ],
    'header'  => [
        'breadcrumbs' => [
            'home' => 'Home',
        ],
    ],
    'login'  => [
        'title' => 'Login to your account',
        'subtitle' => 'Enter your credentials below',
        'email' => 'Email',
        'password' => 'Password',
        'sign_in' => 'Sing in',
    ],
];
