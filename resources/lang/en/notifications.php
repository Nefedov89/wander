<?php

declare(strict_types = 1);

return [
    'user_added' => [
        'subject' => 'Wander profile created',
        'line_1'  => 'Hello! We created Wander profile for you',
        'line_2'  => 'It is your password: :pass',
        'line_3'  => 'You can change it in your profile',
    ],
];
