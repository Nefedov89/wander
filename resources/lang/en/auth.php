<?php

declare(strict_types = 1);

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'               => 'These credentials do not match our records.',
    'throttle'             => 'Too many login attempts. Please try again in :seconds seconds.',
    'not_confirmed'        => 'Sorry, you can not log in unless your account is confirmed. Please follow the instructions you received in email.',
    'verify_email'         => [
        'subject'          => 'Verify Email Address',
        'call_to_action'   => 'Please click the button below to verify your email address.',
        'btn'              => 'Verify Email Address',
        'tip'              => 'If you did not create an account, no further action is required.',
        'verified'         => 'Done! Please login to get start!',
        'already_verified' => 'Your email is already verified',
        'email_resend'     => 'We have sent verification email again. Please check your inbox',
        'not_verified'     => 'Ooops...Email is not verified. Please contact us for further information',
    ],
    'reset_password_email' => [
        'subject' => 'Reset Password Notification',
        'line_1'  => 'You are receiving this email because we received a password reset request for your account.',
        'action'  => 'Reset Password',
        'line_2'  => 'If you did not request a password reset, no further action is required.',
    ],
];
