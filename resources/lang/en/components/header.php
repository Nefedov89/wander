<?php

declare(strict_types = 1);

return [
    'menu'     => [
        'home'       => 'Home',
        'about'      => 'About',
        'my_tours'   => 'My tours',
        'contact_us' => 'Contact us',
        'build_tour' => 'Build my tour',
        'login'      => 'Login',
        'profile'    => 'Profile',
        'logout'     => 'Logout',
        'add_user'   => 'Add user',
    ],
    'messages' => [
        'logout' => 'Goodbye, :name!',
    ],
];
