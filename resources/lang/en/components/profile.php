<?php

declare(strict_types = 1);

return [
    'title'         => 'My Profile',
    'alt_image'     => 'User Avatar',
    'tours_created' => 'tours created',
    'labels'        => [
        'name'             => 'Name',
        'email'            => 'E-mail',
        'password'         => 'Change Password',
        'confirm_password' => 'Confirm New Password',
        'save_changes'     => 'Save Changes',
        'edit_profile'     => 'Edit Profile',
    ],
    'messages'      => [
        'updated' => 'Your profile has been successfully updated',
    ],
];
