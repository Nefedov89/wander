<?php

declare(strict_types = 1);

return [
    'title'           => 'Login',
    'email'           => 'E-mail',
    'password'        => 'Password',
    'btn'             => 'Login',
    'forgot_password' => 'Forgot password',
    'register'        => 'Don\'t have an account',
    'sign_fb'         => 'Sign in with Facebook',
    'sign_google'     => 'Sign in with Google',
    'messages'        => [
        'greeting'      => 'Hi, :name!',
        'authenticated' => 'You are already authenticated!',
    ],
];
