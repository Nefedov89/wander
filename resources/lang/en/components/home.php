<?php

declare(strict_types = 1);

return [
    'title'               => 'Easy build one-day tours with us',
    'build_my_tour'       => 'Build my tour',
    'about'               => [
        'title' => 'About',
        'text'  => 'Imagine that you are travelling and just arrived to new city. You want to explore all of sights 
            but you have only couple of hours. Wander will help you with planning waypoints, transport logistic and optimized routes.
            No more time-consuming planning for your exploring of sights, Wander will do it for you. Enjoy!',
    ],
    'date_and_time'       => [
        'title' => 'Date and Time',
        'text'  => 'You pick date and time for your one-day tour',
    ],
    'choice_of_places'    => [
        'title' => 'Choice of places',
        'text'  => 'You pick places you want to visit. All places are categorized by types',

    ],
    'choice_of_transport' => [
        'title' => 'Choice of transport',
        'text'  => 'You pick transportation mode: public transport, bike, walking, car',
    ],
    'how_it_works'        => [
        'title' => 'How it works',
        'text'  => '<p>All is pretty easy. You should pick the city where you want to see sights, date and time and transportation mode.
            Then you should pick places you want to visit. After this Wander will generate tour for you. That\'s it!</p><p>You can choose an optimized sequence of places to visit on the tour in order to reduce time and distance. In this case, Wander will build a new route for you.</p><p>You can share your tours, so other people also will be able to attend your favorite points of interest. The more likes your tour gets, the higher the rank you get. Collect likes of your tours and become the ultimate sightseeing guide!</p>',
    ],
    'contact_us'          => [
        'title'        => 'Contact us',
        'name'         => 'Name',
        'email'        => 'Email',
        'message'      => 'Message',
        'send_message' => 'Send message',
        'text'         => 'If you have some propositions or wishes to improve Wander please send us an email by filling
            this form or you can email us directly',
    ],
    'messages'            => [
        'contact_us_sent' => 'Thank you :name! We will contact you as soon as possible',
        'joke'            => [
            '1'  => 'Cool, try again!',
            '3'  => 'So proud of you. Keep going!',
            '6'  => 'I see you have serious intentions!',
            '9'  => 'Okay. Now it is not funny. It is not a real phone',
            '11' => 'Please stop this, I can\'t stand it anymore',
            '13' => 'Okay. You won!',
        ],
    ],
];
