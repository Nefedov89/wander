<?php

declare(strict_types = 1);

return [
    'sidebar'     => [
        'filters'  => [
            'title'                      => 'Filters',
            'where'                      => 'Where',
            'city'                       => 'City',
            'current_city'               => 'Your current city',
            'radius'                     => 'Radius',
            'use_current_location'       => 'Use my location',
            'when'                       => 'When',
            'date'                       => 'Date',
            'time'                       => [
                'from' => 'Time start',
                'to'   => 'Time end',
            ],
            'how'                        => 'How',
            'start_from'                 => 'From',
            'use_current_location_start' => 'Use my current location',
            'set_start_point'            => 'Set start point on map',
            'type_from_address'          => 'Or type address of your start',

        ],
        'places'   => [
            'title'             => 'Places',
            'type'              => 'Type',
            'place_type'        => 'Place type (theaters, museums, bars...)',
            'checked_places'    => 'Checked places',
            'load_more'         => 'Load more',
            'start_point'       => 'Start point',
            'filter'            => [
                'title' => 'Filter',
                'label' => 'Filter by name',
            ],
            'search_by_name'    => [
                'label'       => 'Search',
                'placeholder' => 'Enter place\'s name',
            ],
            'types_options'     => [
                'amusement_park'          => 'Amusement park',
                'aquarium'                => 'Aquarium',
                'art_gallery'             => 'Art gallery',
                'bakery'                  => 'Bakery',
                'bar'                     => 'Bar',
                'cafe'                    => 'Cafe',
                'church'                  => 'Church',
                'city_hall'               => 'City hall',
                'courthouse'              => 'Courthouse',
                'hindu_temple'            => 'Hindu temple',
                'library'                 => 'Library',
                'local_government_office' => 'Local government office',
                'mosque'                  => 'Mosque',
                'movie_theater'           => 'Movie theater',
                'museum'                  => 'Museum',
                'park'                    => 'Park',
                'point_of_interest'       => 'Point of interest',
                'stadium'                 => 'Stadium',
                'synagogue'               => 'Synagogue',
                'university'              => 'University',
                'zoo'                     => 'Zoo',
            ],
            'working_hours'     => 'Working hours:',
            'comments'          => [
                'title' => 'Comments',
                'show'  => 'Show comments',
                'hide'  => 'Hide comments',
            ],
            'opening_hours'     => [
                'show'   => 'Show opening hours',
                'hide'   => 'Hide opening hours',
                'opened' => 'Opened now',
                'closed' => 'Closed now',
            ],
            'show_only_checked' => 'Show only checked',
            'show_all'          => 'Show all',
            'want_to_visit'     => 'Want to visit',
            'add_to_tour'       => 'Add to tour',
            'remove_from_tour'  => 'Remove from tour',
            'sort'              => [
                'label'   => 'Sort',
                'tip'     => 'Choose sorting criteria',
                'options' => [
                    'rating' => [
                        'asc'  => 'by rating ascending',
                        'desc' => 'by rating descending',
                    ],
                ],
            ],
        ],
        'generate' => 'Generate tour',
    ],
    'messages'    => [
        'no_location'             => 'Please choose the city or enable Use my location option',
        'no_results'              => 'Sorry. There are no results for :type. Try to search places by name or pick another type',
        'validation'              => [
            'city'           => [
                'coordinates' => 'Please set city',
            ],
            'date'           => 'Please set date',
            'time'           => [
                'from' => 'Please set start time',
                'to'   => 'Please set end time',
            ],
            'how'            => 'Please set how you are going to travel',
            'from'           => 'Please set start point',
            'checked_places' => [
                'min' => 'Please pick at least :count places to visit',
                'max' => 'You can pick maximum :count places',
            ],
        ],
        'set_start'               => 'Please set tour start position using draggable marker',
        'directions_zero_results' => 'Sorry, but we could not find any directions for this set of filters. Try to change transit mode or time',
        'tour_saved'              => 'Tour has been successfully saved',
    ],
    'dialog_text' => 'Wait for it',
];
