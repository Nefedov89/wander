<?php

declare(strict_types = 1);

return [
    'my'       => [
        'title'        => 'All my tours',
        'info'         => [
            'where' => 'Where:',
            'when'  => 'When:',
            'time'  => 'At what time:',
        ],
        'see_details'  => 'See details',
        'no_tours_yet' => 'There are no tours yet',
    ],
    'single'   => [
        'home' => 'Home',
    ],
    'messages' => [
        'deleted'   => 'Tour in :tour has been successfully deleted',
        'not_yours' => 'Oops! Looks like you are trying to delete wrong tour. Contact us for further support.',
    ],
];
