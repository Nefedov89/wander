<?php

declare(strict_types = 1);

return [
    'title'    => 'Add user',
    'email'    => 'E-mail',
    'btn'      => 'Add',
    'messages' => [
        'added'       => 'User :email added',
        'not_allowed' => 'You are not allowed to add user',
    ],
];
