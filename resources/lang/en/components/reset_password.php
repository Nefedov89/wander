<?php

declare(strict_types = 1);

return [
    'email'    => [
        'title'          => 'Forgot Password?',
        'email'          => 'E-mail',
        'reset_password' => 'Reset Password',
    ],
    'reset'    => [
        'title'            => 'Reset Password',
        'password'         => 'New Password',
        'confirm_password' => 'Confirm New Password',
        'reset_password'   => 'Reset Password',
    ],
    'messages' => [
        'authenticated' => 'You are already authenticated!',
        'sent_email'    => 'Done! Check your email to restore password',
        'login'         => 'Good! Please login with your new password',
    ],
];
