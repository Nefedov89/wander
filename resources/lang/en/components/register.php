<?php

declare(strict_types = 1);

return [
    'title'                      => 'Register',
    'name'                       => 'Name',
    'email'                      => 'E-mail',
    'password'                   => 'Password',
    'confirm_password'           => 'Confirm Password',
    'btn'                        => 'Sign in',
    'forgot_password'            => 'Forgot password',
    'login'                      => 'Login',
    'sign_fb'                    => 'Sign in with Facebook',
    'sign_google'                => 'Sign in with Google',
    'accept'                     => 'I accept',
    'agree'                      => 'Agree',
    'disagree'                   => 'Disagree',
    'terms_and_conditions_title' => 'Wander\'s terms and conditions',
    'terms_and_conditions'       => 'Terms and conditions',
    'messages'                   => [
        'check_email' => 'Congratulations! Please, check your email and let\'s build awesome tours!',
        'registered'  => 'You are already registered',
    ],
];
