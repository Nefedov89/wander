<?php

declare(strict_types = 1);

return [
    '404'    => [
        'title'   => '404',
        'message' => 'Sorry, the page you are looking for could not be found',
    ],
    'common' => [
        'home' => 'Go home',
    ],
];
