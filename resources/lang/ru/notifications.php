<?php

declare(strict_types = 1);

return [
    'user_added' => [
        'subject' => 'Wander профиль создан',
        'line_1'  => 'Здравствуйте! Мы создали профиль Wander для вас',
        'line_2'  => 'Это ваш пароль :pass',
        'line_3'  => 'Вы можете изменить это в своем профиле',
    ],
];
