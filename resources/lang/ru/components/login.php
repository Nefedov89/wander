<?php

declare(strict_types = 1);

return [
    'title'           => 'Авторизация',
    'email'           => 'Эл. почта',
    'password'        => 'Пароль',
    'btn'             => 'Авторизоваться',
    'forgot_password' => 'Забыл пароль',
    'register'        => 'Нет аккаунта',
    'sign_fb'         => 'Авторизоваться с помощью Фэйсбука',
    'sign_google'     => 'Войти через Google',
    'messages'        => [
        'greeting'      => 'Привет :name !',
        'authenticated' => 'Вы уже авторизованы!',
    ],
];
