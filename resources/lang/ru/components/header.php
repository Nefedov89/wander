<?php

declare(strict_types = 1);

return [
    'menu'     => [
        'home'       => 'На главную',
        'about'      => 'Про нас',
        'my_tours'   => 'Мои туры',
        'contact_us' => 'Связаться с нами',
        'build_tour' => 'Построить тур',
        'login'      => 'Авторизоваться',
        'profile'    => 'Профиль',
        'logout'     => 'Выйти',
        'add_user'   => 'Добавить пользователя',
    ],
    'messages' => [
        'logout' => 'До встречи, :name !',
    ],
];
