<?php

declare(strict_types = 1);

return [
    '404'    => [
        'title'   => '404',
        'message' => 'Извините, страница, которую вы ищете, не найдена',
    ],
    'common' => [
        'home' => 'На главную',
    ],
];
