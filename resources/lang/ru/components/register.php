<?php

declare(strict_types = 1);

return [
    'title'                      => 'Регистрация',
    'name'                       => 'Имя',
    'email'                      => 'Эл. почта',
    'password'                   => 'Пароль',
    'confirm_password'           => 'Подтвердить пароль',
    'btn'                        => 'Войти в систему',
    'forgot_password'            => 'Забыл пароль',
    'login'                      => 'Авторизоваться',
    'sign_fb'                    => 'Войти с помощью Facebook',
    'sign_google'                => 'Войти С помощью Google',
    'accept'                     => 'Я принимаю',
    'agree'                      => 'Согласен',
    'disagree'                   => 'Не согласен',
    'terms_and_conditions_title' => 'Условия и положения Wander',
    'terms_and_conditions'       => 'Условия и положения',
    'messages'                   => [
        'check_email' => 'Поздравляем! Пожалуйста, проверьте свою электронную почту и давайте строить удивительные туры!',
        'registered'  => 'Вы уже зарегистрированы',
    ],
];
