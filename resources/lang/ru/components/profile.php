<?php

declare(strict_types = 1);

return [
    'title'         => 'Мой профайл',
    'alt_image'     => 'Аватар',
    'tours_created' => 'тур(ов)',
    'labels'        => [
        'name'             => 'Имя',
        'email'            => 'Эл. почта',
        'password'         => 'Изменить пароль',
        'confirm_password' => 'Подтвердите новый пароль',
        'save_changes'     => 'Сохранить изменения',
        'edit_profile'     => 'Редактировать профиль',
    ],
    'messages'      => [
        'updated' => 'Ваш профиль был успешно обновлен',
    ],
];
