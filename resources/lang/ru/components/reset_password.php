<?php

declare(strict_types = 1);

return [
    'email'    => [
        'title'          => 'Забыли пароль?',
        'email'          => 'Эл. почта',
        'reset_password' => 'Сброс пароля',
    ],
    'reset'    => [
        'title'            => 'Сброс пароля',
        'password'         => 'Новый пароль',
        'confirm_password' => 'Подтвердить новый пароль',
        'reset_password'   => 'Сброс пароля',
    ],
    'messages' => [
        'authenticated' => 'Вы уже авторизованы!',
        'sent_email'    => 'Готово! Проверьте свою электронную почту, чтобы восстановить пароль',
        'login'         => 'Готово! Пожалуйста, войдите под своим новым паролем',
    ],
];
