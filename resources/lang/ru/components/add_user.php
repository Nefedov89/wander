<?php

declare(strict_types = 1);

return [
    'title'    => 'Добавить юзера',
    'email'    => 'Эл. почта',
    'btn'      => 'Добавить',
    'messages' => [
        'added'       => 'Пользователь :email добавлен',
        'not_allowed' => 'Вы не можете добавить пользователя',
    ],
];
