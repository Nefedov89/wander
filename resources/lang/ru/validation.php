<?php

declare(strict_types = 1);

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute должен быть принят.',
    'active_url'           => ':attribute : не является допустимым URL.',
    'after'                => ':attribute должен быть датой после :date .',
    'after_or_equal'       => ':attribute должен быть датой после или равен :date .',
    'alpha'                => ':attribute может содержать только буквы.',
    'alpha_dash'           => ':attribute может содержать только буквы, цифры, тире и подчеркивание.',
    'alpha_num'            => ':attribute может содержать только буквы и цифры.',
    'array'                => ':attribute должен быть массивом.',
    'before'               => ':attribute должен быть датой до :date .',
    'before_or_equal'      => ':attribute должен быть датой до или равен :date .',
    'between'              => [
        'numeric' => ':attribute должен быть в пределах :min и :max .',
        'file'    => ':attribute должен быть в пределах :min и :max килобайт.',
        'string'  => ':attribute должен быть в пределах :min и :max знаков',
        'array'   => ':attribute должен иметь значения между :min и :max .',
    ],
    'boolean'              => 'Поле :attribute должно быть истинным или ложным.',
    'confirmed'            => ':attribute подтверждения не совпадает.',
    'date'                 => ':attribute : не является допустимой датой.',
    'date_format'          => ':attribute не соответствует формату :format .',
    'different'            => ':attribute и :other должны быть разными.',
    'digits'               => ':attribute должен быть :digits цифры.',
    'digits_between'       => ':attribute должен быть в пределах :min и :max цифр.',
    'dimensions'           => ':attribute имеет недопустимые размеры изображения.',
    'distinct'             => 'Поле :attribute имеет повторяющееся значение.',
    'email'                => ':attribute должен быть действительным адресом электронной почты.',
    'exists'               => ':attribute selected :attribute недействителен.',
    'file'                 => ':attribute должен быть файлом.',
    'filled'               => 'Поле :attribute должно иметь значение.',
    'gt'                   => [
        'numeric' => ':attribute должен быть больше чем :value .',
        'file'    => ':attribute должен быть больше чем :value килобайт.',
        'string'  => ':attribute должен быть больше чем :value знаков.',
        'array'   => ':attribute должен иметь больше чем :value элементов.',
    ],
    'gte'                  => [
        'numeric' => ':attribute должен быть больше или равен :value',
        'file'    => ':attribute должен быть больше или равен :value килобайт.',
        'string'  => ':attribute должен быть больше или равен :value знаков.',
        'array'   => ':attribute должен иметь :value элементы :value или более.',
    ],
    'image'                => ':attribute должен быть изображением.',
    'in'                   => ':attribute selected :attribute недействителен.',
    'in_array'             => 'Поле :attribute не существует в :other .',
    'integer'              => ':attribute должен быть целым числом.',
    'ip'                   => ':attribute должен быть действительным IP-адресом.',
    'ipv4'                 => ':attribute должен быть действительным адресом IPv4.',
    'ipv6'                 => ':attribute должен быть действительным адресом IPv6.',
    'json'                 => ':attribute должен быть допустимой строкой JSON.',
    'lt'                   => [
        'numeric' => ':attribute должен быть меньше чем :value .',
        'file'    => ':attribute должен быть меньше чем :value килобайт.',
        'string'  => ':attribute должен быть меньше чем :value знаков.',
        'array'   => ':attribute должен иметь меньше чем :value элементов.',
    ],
    'lte'                  => [
        'numeric' => ':attribute должен быть меньше или равен :value',
        'file'    => ':attribute должен быть меньше или равен :value килобайт.',
        'string'  => ':attribute должен быть меньше или равен :value знаков.',
        'array'   => ':attribute не должен содержать больше, чем :value элементов.',
    ],
    'max'                  => [
        'numeric' => ':attribute не может быть больше, чем :max .',
        'file'    => ':attribute не может быть больше, чем :max килобайт.',
        'string'  => ':attribute не может быть больше, чем :max знаков.',
        'array'   => ':attribute может содержать не более :max .',
    ],
    'mimes'                => ':attribute должен быть файлом.of type: :values.',
    'mimetypes'            => ':attribute должен быть файлом.of type: :values.',
    'min'                  => [
        'numeric' => ':attribute должен быть не менее :min',
        'file'    => ':attribute должен быть не менее :min килобайт',
        'string'  => ':attribute должен быть не менее :min знаков.',
        'array'   => ':attribute должен содержать как минимум :min элементов.',
    ],
    'not_in'               => ':attribute selected :attribute недействителен.',
    'not_regex'            => 'Формат :attribute : неверен.',
    'numeric'              => ':attribute должен быть числом.',
    'present'              => 'Поле :attribute должно присутствовать.',
    'regex'                => 'Формат :attribute : неверен.',
    'required'             => 'Поле :attribute обязательно для заполнения.',
    'required_if'          => 'Поле :attribute обязательно для заполнения когда значение :other равно :value.',
    'required_unless'      => 'Поле :attribute обязательно для заполнения если поле :other равно :values.',
    'required_with'        => 'Поле :attribute обязательно для заполнения когда значение :values присутствует.',
    'required_with_all'    => 'Поле :attribute обязательно для заполнения когда значение :values присутствует.',
    'required_without'     => 'Поле :attribute обязательно для заполнения когда значение :values не присутствует.',
    'required_without_all' => 'Поле :attribute обязательно для заполнения когда ни одно из значений :values не присутствует.',
    'same'                 => ':attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => ':attribute должен быть :size .',
        'file'    => ':attribute должен быть :size килобайт.',
        'string'  => ':attribute должен быть :size знаков.',
        'array'   => ':attribute должен содержать :size элементов.',
    ],
    'string'               => ':attribute должен быть строкой.',
    'timezone'             => ':attribute должен быть допустимой зоной.',
    'unique'               => ':attribute уже занят.',
    'uploaded'             => 'Не удалось загрузить :attribute',
    'url'                  => 'Формат :attribute : неверен.',
    'custom' => [
        'time_end' => [
            'after' => 'Время финиша должно быть позже, чем время старта',
        ],
    ],
];
