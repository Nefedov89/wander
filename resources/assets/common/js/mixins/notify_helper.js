export default {
  mounted() {
    const type = _.get(wander, 'notification.type');
    const text = _.get(wander, 'notification.text');

    if (type && text) {
      if (notify.hasOwnProperty(type) && typeof notify[type] === 'function') {
        notify[type](text, '');
      }
    }
  },
};
