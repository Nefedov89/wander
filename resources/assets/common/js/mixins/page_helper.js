export default {
  data() {
    return {
      modals: {
        info: {
          title: null,
          text: null,
        },
      },
    };
  },

  methods: {
    showInfoModal(title, text) {
      this.modals.info.title = title;
      this.modals.info.text = text;

      this.$bvModal.show('info');
    },

    doCopy(string) {
      this.$copyText(string).then((e) => {
        notify.success(
          this.$t('general.word.copied')
        );
      }, (e) => {
        notify.success(
          this.$t('general.word.not_copied')
        );
      });
    },
  },
};
