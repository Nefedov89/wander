import { DateTime } from 'luxon';

export default {
  methods: {
    formatDate(date, format) {
      return DateTime.fromSQL(date).toFormat(format);
    },

    formatJsDate(date, format) {
      return DateTime.fromJSDate(date).toFormat(format);
    },
  },
};
