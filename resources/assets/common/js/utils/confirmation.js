import swal from 'sweetalert';

// todo: translations
const swalConfig = (data) => {
  return {
    title: data.title || 'Are you sure?',
    text: data.text || 'You will not be able to recover this',
    type: data.type || 'warning',
    showCancelButton: data.showCancelButton || true,
    confirmButtonColor: data.confirmButtonColor || '#FF7043',
    confirmButtonText: data.confirmButtonText || 'Yes, proceed!',
    cancelButtonText: data.cancelButtonText || 'Cancel',
  };
};
const confirmation = {};

confirmation.delete = (
  success,
  data = {}
) => {
  if (!data.confirmButtonText) {
    data.confirmButtonText = 'Yes, delete it!';
  }

  swal(swalConfig(data), success);
};

confirmation.warning = (
  success,
  data = {}
) => {
  swal(swalConfig(data), success);
};

window.confirmation = confirmation;
