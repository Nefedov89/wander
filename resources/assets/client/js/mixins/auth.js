import { mapGetters, mapActions } from 'vuex';

export default {
  data() {
    return {
      tokenRefreshBufferInSeconds: 240,
    };
  },

  computed: {
    ...mapGetters([
      'isAuthenticated',
      'token',
      'tokenExpiresAt',
    ]),
  },

  watch: {
    token(token) {
      this.adjustAuthHeader(token);
    },
  },

  methods: {
    isTokenExpired() {
      const nowTs = DateTime.utc().ts;
      const tokenExpiresAtTs  = DateTime.fromSeconds(this.tokenExpiresAt).ts;

      return (nowTs - tokenExpiresAtTs) > 0;
    },

    adjustAuthHeader(token) {
      if (token) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      } else {
        delete axios.defaults.headers.common['Authorization'];
      }
    },

    handleUnauthorized() {
      this.notifyWarning(this.$i18n.translate('common.messages.anon'));
      this.$router.push({ name: 'login' });
    },

    handleExpiredToken() {
      this.logout();
      this.$router.push({ name: 'home' });
    },

    handleRefreshToken() {
      const nowTs = DateTime.utc().ts;
      const tokenExpiresAtTs  = DateTime.fromSeconds(this.tokenExpiresAt).ts;
      const shouldRefresh = (tokenExpiresAtTs - nowTs) / 1000 < this.tokenRefreshBufferInSeconds;

      if (this.token && !this.isTokenExpired() && shouldRefresh) {
        axios.get(
          Router.route('api.auth.refresh-token'),
        ).then(({ data }) => {
          this.login({
            user: JSON.stringify(data.user),
            token: data.token,
            token_expires_at: data.token_expires_at,
          });
        });
      }
    },

    redirectIfAuthenticated() {
      if (this.isAuthenticated) {
        this.notifyInfo(this.$i18n.translate('common.messages.authenticated'));

        this.$router.push({ name: 'home' });
      }
    },

    ...mapActions([
      'login',
      'logout',
      'notifyWarning',
      'notifyInfo',
    ]),
  },

  created() {
    this.adjustAuthHeader(this.token);
  },

  mounted() {
    // Handle token refreshing after direct page loading.
    this.handleRefreshToken();

    // Check auth after direct page loading.
    if (this.$route.meta.auth && !this.isAuthenticated) {
      this.handleUnauthorized();
    }

    // Check token expired after direct page loading.
    if (this.token && this.isTokenExpired()) {
      this.handleExpiredToken();
    }

    this.$router.beforeEach((to, from, next) => {
      // Handle token refreshing during vue router navigation.
      this.handleRefreshToken();

      if (to.meta.auth && !this.isAuthenticated) {
        // Check auth for each route during vue router navigation.
        this.handleUnauthorized();
      } else if (this.token && this.isTokenExpired()) {
        // Check token expired during vue router navigation.
        this.handleExpiredToken();
      } else {
        next();
      }
    });
  },
};
