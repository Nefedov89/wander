export default {
  methods: {
    scrollToAnchor(id) {
      const $elementToScrollTo = $(`#${id}`)[0];

      if ($elementToScrollTo !== undefined) {
        $elementToScrollTo.scrollIntoView({
          behavior: 'smooth'
        });
      }
    },
  },
};
