import { mapActions } from 'vuex';

export default {
  methods: {
    ...mapActions([
      'setUser',
      'setToken',
      'setTokenExpiresAt',
      'setCurrentLocation',
      'setLang',
    ]),
  },

  created() {
    const user = JSON.parse(window.WebStorage.getItem('user'));

    this.setUser(user || null);
    this.setToken(window.WebStorage.getItem('token') || null);
    this.setTokenExpiresAt(Number(window.WebStorage.getItem('token_expires_at')) || 0);
    this.setCurrentLocation();
    this.setLang(user ? user.language : (window.WebStorage.getItem('lang') || 'ru'));
  },
};
