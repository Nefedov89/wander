export default {
  created() {
    const pageReloadAlertHandler = (e) => {
      const confirmationMessage = this.$i18n.translate('components.common.messages.close');

      (e || window.event).returnValue = confirmationMessage;

      return confirmationMessage;
    };

    if (window.location.pathname === '/build-tour') {
      window.addEventListener('beforeunload', pageReloadAlertHandler);
    } else {
      window.removeEventListener('beforeunload', pageReloadAlertHandler);
    }

    this.$router.beforeEach((to, from, next) => {
      if (to.name === 'build-tour') {
        window.addEventListener('beforeunload', pageReloadAlertHandler);
      } else {
        window.removeEventListener('beforeunload', pageReloadAlertHandler);
      }

      VueBus.$emit('closeMenu');

      next();
    });
  },
};
