export default {
  data() {
    return {
      errors: {},
      errorMessage: null,
      loader: null,
      loading: false,
    };
  },

  watch: {
    loader() {
      const l = this.loader;
      this[l] = !this[l];
    },
  },

  methods: {
    submitOnInput(e, callback) {
      if (e.keyCode === 13 && typeof this[callback] === 'function') {
        this[callback]();
      }
    },

    clearErrors() {
      this.errors = {};
      this.errorMessage = null;
    },

    clearModels(form = 'models') {
      _.each(this[form], (value, key) => {
        this[form][key] = null;
      });
    },

    stopLoader() {
      this.loader = null;
      this.loading = false;
    },
  },
};
