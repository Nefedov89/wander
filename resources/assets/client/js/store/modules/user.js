const state = {
  user: null,
  token: null,
  token_expires_at: 0,
  lang: 'ru',
};

const getters = {
  user: (state) => {
    return state.user;
  },

  token: (state) => {
    return state.token;
  },

  tokenExpiresAt: (state) => {
    return state.token_expires_at;
  },

  lang: (state) => {
    return state.lang;
  },

  isAuthenticated: (state) => {
    return state.user !== null && state.token !== null;
  },
};

const mutations = {
  setUser: (state, user) => {
    state.user = user;
  },

  setToken: (state, token) => {
    state.token = token;
  },

  setTokenExpiresAt: (state, timestamp) => {
    state.token_expires_at = timestamp;
  },

  unsetUser: (state) => {
    state.user = null;
  },

  unsetToken: (state) => {
    state.token = null;
    state.token_expires_at = 0;
  },

  setLang: (state, lang) => {
    state.lang = lang;
  },
};

const actions = {
  setUser: ({ commit }, user) => {
    window.WebStorage.setItem('user', JSON.stringify(user));
    commit('setUser', user);
  },

  setToken: ({ commit }, token) => {
    window.WebStorage.setItem('token', token);
    commit('setToken', token);
  },

  setTokenExpiresAt: ({ commit }, timestamp) => {
    window.WebStorage.setItem('token_expires_at', timestamp);
    commit('setTokenExpiresAt', timestamp);
  },

  unsetUser: ({ commit }) => {
    window.WebStorage.removeItem('user');
    commit('unsetUser');
  },

  unsetToken: ({ commit }) => {
    window.WebStorage.removeItem('token');
    window.WebStorage.removeItem('token_expires_at');
    commit('unsetToken');
  },

  setLang: ({ commit }, lang) => {
    window.WebStorage.setItem('lang', lang);
    commit('setLang', lang);
  },

  login: ({ dispatch }, data) => {
    const user = JSON.parse(data.user);

    dispatch('setUser', user);
    dispatch('setToken', data.token);
    dispatch('setTokenExpiresAt', data.token_expires_at);
    dispatch('setLang', user.language);
  },

  logout: ({ dispatch }) => {
    dispatch('unsetUser');
    dispatch('unsetToken');
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
