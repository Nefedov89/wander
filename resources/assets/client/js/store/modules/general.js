import { isMobile } from 'mobile-device-detect';

const state = {
  isMobile,
  notification: {
    text: '',
    show: false,
    timeout: 7000,
    color: 'info',
  },
  currentLocation: null,
  icons: {
    defaultMarkerIconUrl: '/assets/client/images/marker_unchecked.svg',
    checkedMarkerIconUrl: '/assets/client/images/marker_checked.svg',
    startMarkerIconUrl: '/assets/client/images/marker_start.svg',
    currentPositionIconUrl: '/assets/client/images/marker_current_position.svg',
  },
  navigatorPositionWatcherId: null,
  transportationModesMap: {
    TRANSIT: {
      icon: 'directions_bus',
      key: 'public_transport',
    },
    DRIVING: {
      icon: 'directions_car',
      key: 'personal_car',
    },
    WALKING: {
      icon: 'directions_walk',
      key: 'walk',
    },
    BICYCLING: {
      icon: 'directions_bike',
      key: 'bicycle',
    },
  },
};

const getters = {
  isMobile: (state) => {
    return state.isMobile;
  },

  notification: (state) => {
    return state.notification;
  },

  currentLocation: (state) => {
    return state.currentLocation;
  },

  icons: (state) => {
    return state.icons;
  },

  navigatorPositionWatcherId: (state) => {
    return state.navigatorPositionWatcherId;
  },

  transportationModesMap: (state) => {
    return state.transportationModesMap;
  },
};

const mutations = {
  notify: (state, data) => {
    state.notification.text = data.text;
    state.notification.color = data.type;
    state.notification.show = true;
  },

  setCurrentLocation: (state) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          state.currentLocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
        },
        (error) => {
          console.warn(`geo error(${error.code}): ${error.message}`);
        },
        { enableHighAccuracy: true }
      );
    } else {
      console.error('geo location is not supported');
    }
  },

  watchCurrentLocation: (state) => {
    state.navigatorPositionWatcherId = navigator.geolocation.watchPosition(
      (position) => {
        state.currentLocation.lat = position.coords.latitude;
        state.currentLocation.lng = position.coords.longitude;
      },
      (error) => {
        console.warn('ERROR(' + error.code + '): ' + error.message);
      },
      {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      }
    );
  },
};

const actions = {
  notifySuccess: ({ commit }, text) => {
    commit('notify', {
      text,
      type: 'success',
    });
  },

  notifyInfo: ({ commit }, text) => {
    commit('notify', {
      text,
      type: 'info',
    });
  },

  notifyError: ({ commit }, text) => {
    commit('notify', {
      text,
      type: 'error',
    });
  },

  notifyWarning: ({ commit }, text) => {
    commit('notify', {
      text,
      type: 'warning',
    });
  },

  setCurrentLocation: ({ commit }) => {
    commit('setCurrentLocation');
  },

  watchCurrentLocation: ({ commit }) => {
    commit('watchCurrentLocation');
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
