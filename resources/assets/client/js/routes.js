import Home from './components/pages/Home';
import Login from './components/pages/Login';
import Register from './components/pages/Register';
import ResetPasswordEmail from './components/pages/ResetPasswordEmail';
import ResetPassword from './components/pages/ResetPassword';
import BuildTour from './components/pages/BuildTour';
import MyTours from './components/pages/MyTours';
import Tour from './components/pages/Tour';
import Profile from './components/pages/Profile';
import TermsAndConditions from './components/pages/TermsAndConditions';
import NotFound from './components/pages/NotFound';
import AddUser from './components/pages/AddUser';

export const routes = [
  {
    path: '*',
    component: NotFound,
    meta: { auth: false },
  },
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: { auth: false },
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: { auth: false },
  },
  // {
  //   path: '/register',
  //   name: 'register',
  //   component: Register,
  //   meta: { auth: false },
  // },
  {
    path: '/reset-password/email',
    name: 'reset-password-email',
    component: ResetPasswordEmail,
    meta: { auth: false },
  },
  {
    path: '/reset-password/:token',
    name: 'reset-password',
    component: ResetPassword,
    meta: { auth: false },
  },
  {
    path: '/build-tour',
    name: 'build-tour',
    component: BuildTour,
    meta: { auth: true },
  },
  {
    path: '/my-tours',
    name: 'my-tours',
    component: MyTours,
    meta: { auth: true },
  },
  {
    path: '/tour/:id',
    name: 'tour',
    component: Tour,
    meta: { auth: true },
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    meta: { auth: true },
  },
  {
    path: '/terms-and-conditions',
    name: 'terms-and-conditions',
    component: TermsAndConditions,
    meta: { auth: false },
  },
  {
    path: '/add-user',
    name: 'add-user',
    component: AddUser,
    meta: { auth: true },
  },
];
