import Vue from 'vue';
import Vuex from 'vuex';
import general from './store/modules/general';
import user from './store/modules/user';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    general,
    user,
  },
});
