require('../../common/js/bootstrap');
require('./components');

import Vue from 'vue';
import VueRouter from 'vue-router';
import { routes } from './routes';
import Vuetify from 'vuetify';
import { store } from './store';
import 'es6-promise/auto';
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import 'swiper/dist/css/swiper.css';
import StoreMixin from './mixins/store';
import AuthMixin from './mixins/auth';
import GeneralMixin from './mixins/general';
import VuexI18n from 'vuex-i18n';
import Locales from '../../common/js/generated/i18n-messages';
import { mapGetters } from 'vuex';
import * as VueGoogleMaps from 'vue2-google-maps';
import Vue2TouchEvents from 'vue2-touch-events';
import config from './config';
import confirmation from './utils/confirmation';

// Font awesome.
Vue.component('font-awesome-icon', FontAwesomeIcon);
library.add(fas, fab);

// Vue Router.
Vue.use(VueRouter);

window.VueBus = new Vue();

const router = new VueRouter({
  routes,
  mode: 'history',
  linkExactActiveClass: 'active',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }

    if (to.hash) {
      return { selector: to.hash };
    }
  },
});

// Vue i18n.
Vue.use(VuexI18n.plugin, store);
_.each(['en', 'ru', 'ua', 'de'], (lang) => {
  Vue.i18n.add(lang, Locales[lang]);
});

// Vuetify.
Vue.use(Vuetify, {
  theme: {
    primary: '#009ada',
    secondary: '#ff9419',
  },
});

// VueAwesomeSwiper.
Vue.use(VueAwesomeSwiper);

// Vue2TouchEvents.
Vue.use(Vue2TouchEvents);

new Vue({
  el: '#app',
  iconfont: 'faSvg',
  router,
  store,

  mixins: [StoreMixin, AuthMixin, GeneralMixin],

  computed: {
    ...mapGetters([
      'lang',
    ]),
  },

  watch: {
    lang(newLang) {
      if (newLang !== Vue.i18n.locale()) {
        Vue.i18n.set(newLang);
      }
    },
  },

  created() {
    // Set language.
    Vue.i18n.set(this.lang);

    // Google geo location.
    Vue.use(VueGoogleMaps, {
      load: {
        key: config.google_api_key,
        libraries: 'places,geometry,directions',
        language: navigator.language,
      },
    });
  },
});

// Lodash for usage in templates.
Object.defineProperty(Vue.prototype, '$_', { value: window._ });

// Confirmations.
window.confirmation = confirmation;
