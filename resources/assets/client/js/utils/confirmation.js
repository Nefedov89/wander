import Swal from 'sweetalert2';

// todo: translations
const swalConfig = (data) => {
  return {
    title: data.title || 'Are you sure?',
    text: data.text || 'You will not be able to recover this',
    icon: data.type || 'warning',
    showCancelButton: data.showCancelButton || true,
    confirmButtonColor: data.confirmButtonColor || '#FF7043',
    confirmButtonText: data.confirmButtonText || 'Yes, proceed!',
    cancelButtonText: data.cancelButtonText || 'Cancel',
  };
};
const confirmation = {};

confirmation.delete = (
    successCallback,
    data = {}
) => {
    if (!data.confirmButtonText) {
        data.confirmButtonText = 'Yes, delete it!';
    }

    Swal.fire(swalConfig(data)).then((result) => {
        if (!result.value) {
           return;
        }

        successCallback();
    });
};

confirmation.warning = (
    successCallback,
    data = {}
) => {
    Swal.fire(swalConfig(data)).then((result) => {
        if (!result.value) {
            return;
        }

        successCallback();
    });
};

export default confirmation;
