import Vue from 'vue';
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

const context = require.context(
  './components/common/',
  true,
  /\.vue$/
);

context.keys().forEach((fileName) => {
  const componentConfig = context(fileName);
  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1')),
  );

  Vue.component(
    componentName,
    componentConfig.default || componentConfig,
  );
});
