import DateHelper from '../../../common/js/mixins/date_helper';
import DateTime from 'luxon/src/datetime.js';

export default {
  mixins: [DateHelper],

  data() {
    return {

    };
  },

  methods: {
    getQueryParams() {
      const parameters = _.pickBy(this.filters, _.identity);

      if (parameters.date_value !== null) {
        const dateRange = this.handleDateFilter(parameters.date_value);

        if (dateRange) {
          parameters.date_range = dateRange;
        }
      }

      return parameters;
    },

    highlightSearchResult(resultString, queryString) {
      const regex = new RegExp(_.escapeRegExp(queryString), 'gi');

      return resultString !== undefined
        ? resultString.replace(regex, (substring) => {
          return `<strong>${substring}</strong>`;
        })
        : '';
    },

    getWrappedName(name, filter = 'name', shouldTruncate = true) {
      const tName = shouldTruncate ? _.truncate(name, { length: 50 }) : name;

      return this.filters[filter]
        ? this.highlightSearchResult(tName, this.filters[filter])
        : tName;
    },

    handleDateFilter(key) {
      let range = null;

      switch (key) {
        case 'today':
          range = {
            from: DateTime.utc().toSQLDate(),
            to: DateTime.utc().toSQLDate(),
          };

          break;
        case 'tomorrow':
          range = {
            from: DateTime.utc()
              .plus({ days: 1 })
              .toSQLDate(),
            to: DateTime.utc()
              .plus({ days: 1 })
              .toSQLDate(),
          };

          break;
        case 'this_week':
          range = {
            from: DateTime.utc()
              .startOf('week')
              .toSQLDate(),
            to: DateTime.utc()
              .endOf('week')
              .toSQLDate(),
          };
          break;
        case 'this_month':
          range = {
            from: DateTime.utc()
              .startOf('month')
              .toSQLDate(),
            to: DateTime.utc()
              .endOf('month')
              .toSQLDate(),
          };
          break;
        case 'next_7_days':
          range = {
            from: DateTime.utc().toSQLDate(),
            to: DateTime.utc()
              .plus({ days: 7 })
              .toSQLDate(),
          };

          break;
        case 'next_30_days':
          range = {
            from: DateTime.utc().toSQLDate(),
            to: DateTime.utc()
              .plus({ days: 30 })
              .toSQLDate(),
          };

          break;
        case 'all_upcoming':
          range = {
            from: DateTime.utc().toSQLDate(),
            to: DateTime.utc()
              .plus({ years: 100 })
              .toSQLDate(),
          };

          break;
        case 'yesterday':
          range = {
            from: DateTime.utc()
              .minus({ days: 1 })
              .toSQLDate(),
            to: DateTime.utc()
              .minus({ days: 1 })
              .toSQLDate(),
          };

          break;
        case 'last_week':
          range = {
            from: DateTime.utc()
              .minus({ weeks: 1 })
              .startOf('week')
              .toSQLDate(),
            to: DateTime.utc()
              .minus({ weeks: 1 })
              .endOf('week')
              .toSQLDate(),
          };

          break;
        case 'last_month':
          range = {
            from: DateTime.utc()
              .minus({ months: 1 })
              .startOf('month')
              .toSQLDate(),
            to: DateTime.utc()
              .minus({ months: 1 })
              .endOf('month')
              .toSQLDate(),
          };

          break;
        case 'last_7_days':
          range = {
            from: DateTime.utc()
              .minus({ days: 7 })
              .toSQLDate(),
            to: DateTime.utc().toSQLDate(),
          };

          break;
        case 'last_30_days':
          range = {
            from: DateTime.utc()
              .minus({ days: 30 })
              .toSQLDate(),
            to: DateTime.utc().toSQLDate(),
          };

          break;
        case 'all_past':
          range = {
            from: DateTime.utc()
              .minus({ years: 100 })
              .toSQLDate(),
            to: DateTime.utc().toSQLDate(),
          };

          break;
        case 'custom':
          const startDate = this.filters.date_custom.startDate;
          const endDate = this.filters.date_custom.endDate;

          if (startDate && endDate) {
            range = {
              from: DateTime
                .fromISO(this.filters.date_custom.startDate.toISOString())
                .toSQLDate(),
              to: DateTime
                .fromISO(this.filters.date_custom.endDate.toISOString())
                .toSQLDate(),
            };
          }

          break;
      }

      return range ? JSON.stringify(range) : null;
    },

    getUserSchoolField(user, field) {
      let value = '';

      if (user.school && user.school.hasOwnProperty(field)) {
        value = user.school[field];
      }

      return value;
    },

    getTeacherProfileField(user, field) {
      let value = '';

      if (user.teacher_profile && user.teacher_profile.hasOwnProperty(field)) {
        value = user.teacher_profile[field];
      }

      return value;
    },
  },
};
