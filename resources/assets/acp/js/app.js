require('../../common/js/bootstrap');

window.Vue = require('vue');

import extendVue from '../../common/js/extensions/vue';
import components from './components';
import DeleteConfirmation from '../../common/js/components/delete';
import SearchModel from './components/common/search';
import Spinner from '../../common/js/components/spinner';
import BootstrapVue from 'bootstrap-vue';
import VueSelect from 'vue-select';
import i18n from '../../common/js/utils/i18n';
import 'vue-select/dist/vue-select.css';
import 'sweetalert/dist/sweetalert.css';
import DateRangePicker from 'vue2-daterange-picker';
import 'vue2-daterange-picker/dist/vue2-daterange-picker.css';
import VueClipboard from 'vue-clipboard2';
import NotifyHelper from '../../common/js/mixins/notify_helper';

extendVue(Vue);

// Global components.
Vue.component('vue-select', VueSelect);
Vue.component('delete-confirmation', DeleteConfirmation);
Vue.component('search-model', SearchModel);
Vue.component('spinner', Spinner);
Vue.component('date-range-picker', DateRangePicker);

Vue.use(BootstrapVue);
Vue.use(VueClipboard);

export const eventBus = new Vue();

const app = new Vue({
  el: '#app',
  i18n,
  components,
  mixins: [NotifyHelper],
});

Object.defineProperty(Vue.prototype, '$_', { value: window._ });

