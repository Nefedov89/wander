import NotificationsBadge from './components/notification/badge';
import UserIndex from './components/user/index';
import UserCreate from './components/user/create';
import UserEdit from './components/user/edit';

export default {
  NotificationsBadge,
  UserIndex,
  UserCreate,
  UserEdit,
};
