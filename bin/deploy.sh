#!/usr/bin/env bash

cd /home/forge/wander.cool
git pull origin master
composer install --no-interaction --prefer-dist --optimize-autoloader
echo "" | sudo -S service php7.2-fpm reload

if [[ -f artisan ]]
then
    php artisan down
    php artisan migrate --force
    php artisan config:clear
    php artisan cache:clear
    php artisan view:cache
    php artisan queue:restart
    php artisan vue-i18n:generate
    php artisan laroute:generate
    php artisan up
fi

npm install
npm run prod


