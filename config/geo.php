<?php

declare(strict_types = 1);

return [
    'api_key' => env('GEO_API_KEY'),
];
