<?php

declare(strict_types = 1);

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use const true, false;
use function bcrypt, factory, json_decode;

/**
 * Class ProfileTest
 *
 * @package Tests\Unit
 */
class ProfileTest extends TestCase
{
    use DatabaseMigrations;

    public const NAME = 'Mark Menson';

    public const EMAIL = 'mark_menson@mail.com';

    public const EXISTING_EMAIL = 'john_doe@mail.com';

    public const PASSWORD = 'qwerty';

    public const NEW_NAME = 'Derek Smith';

    public const NEW_EMAIL = 'derek_smith@mail.com';

    public const NEW_PASSWORD = '123456';

    public const WRONG_CONFIRMATION_PASSWORD = 'qwerty';

    public const URL_PROFILE_UPDATE = '/api/profile';

    public const URL_LOGIN = '/api/login';

    public const URL_LOGOUT = '/api/logout';

    /** @var \App\Models\User|null */
    protected $user = null;

    /** @var string|null */
    protected $token = null;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $user = factory(User::class)->create([
            'email'    => static::EMAIL,
            'password' => bcrypt(static::PASSWORD),
        ]);
        $user->markEmailAsVerified();
        $this->user = $user;
        $this->token = Auth::tokenById($user->getKey());
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateProfilePositive(): void
    {
        $response = $this->json(
            'POST',
            static::URL_PROFILE_UPDATE,
            [
                'name'                  => static::NEW_NAME,
                'email'                 => static::NEW_EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => static::NEW_PASSWORD,
                '_method'               => 'PATCH',
            ],
            [
                'Authorization' => 'Bearer '.$this->token,
            ]
        );

        $response
            ->assertStatus(HttpResponse::HTTP_OK)
            ->assertJsonStructure(['user']);

        $response = $this->json(
            'POST',
            static::URL_LOGIN,
            [
                'email'    => static::NEW_EMAIL,
                'password' => static::NEW_PASSWORD,
            ]
        );
        $response
            ->assertStatus(HttpResponse::HTTP_OK)
            ->assertJsonStructure(['token', 'user', 'token_expires_at']);

        $this->assertTrue(
            json_decode($response->getContent())->user->id === $this->user->id
        );
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateProfileNegative(): void
    {
        $wrongCredentialsSet = [
            [
                'email'   => static::EXISTING_EMAIL,
                '_method' => 'PATCH',
            ],
            [
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => static::WRONG_CONFIRMATION_PASSWORD,
                '_method'               => 'PATCH',
            ],
            [
                'password' => static::NEW_PASSWORD,
                '_method'  => 'PATCH',
            ],
            [
                'password_confirmation' => static::NEW_PASSWORD,
                '_method'               => 'PATCH',
            ],
        ];

        foreach ($wrongCredentialsSet as $credentials) {
            $response = $this->json(
                'POST',
                static::URL_PROFILE_UPDATE,
                $credentials
            );
            $response
                ->assertStatus(HttpResponse::HTTP_UNAUTHORIZED);

            $response = $this->json(
                'POST',
                static::URL_PROFILE_UPDATE,
                $credentials,
                [
                    'Authorization' => 'Bearer '.$this->token,
                ]
            );
            $response
                ->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY)
                ->assertJsonStructure(['code', 'errors']);
        }
    }
}
