<?php

declare(strict_types = 1);

namespace Tests\Unit;

use App\Models\User;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyEmailNotification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use const true, false;
use function bcrypt, factory;

/**
 * Class AuthTest
 *
 * @package Tests\Unit
 */
class AuthTest extends TestCase
{
    use DatabaseMigrations;

    public const NAME = 'John Doe';

    public const EMAIL = 'test_user@mail.com';

    public const INVALID_EMAIL = 'test_user';

    public const FAKE_EMAIL = 'fake_test_user@mail.com';

    public const NEW_USER_EMAIL = 'john_doe@mail.com';

    public const PASSWORD = 'password';

    public const NEW_PASSWORD = 'qwerty';

    public const FAKE_PASSWORD = 'test123';

    public const URL_LOGIN = '/api/login';

    public const URL_LOGOUT = '/api/logout';

    public const URL_REGISTER = '/api/register';

    public const URL_TOKEN_REFRESH = '/api/refresh-token';

    public const URL_RESET_PASSWORD_EMAIL = '/api/reset-password/email';

    public const URL_RESET_PASSWORD = '/api/reset-password';

    /** @var \App\Models\User|null */
    protected $user = null;

    /** @var string|null */
    protected $token = null;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        Notification::fake();

        $user = factory(User::class)->create([
            'email'    => static::EMAIL,
            'password' => bcrypt(static::PASSWORD),
        ]);
        $user->markEmailAsVerified();
        $this->user = $user;
        $this->token = Auth::tokenById($user->getKey());
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testLoginPositive(): void
    {
        $response = $this->json(
            'POST',
            static::URL_LOGIN,
            [
                'email'    => $this->user->getAttribute('email'),
                'password' => static::PASSWORD,
            ]
        );

        $response
            ->assertStatus(HttpResponse::HTTP_OK)
            ->assertJsonStructure(['token', 'user', 'token_expires_at']);
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testLoginNegative(): void
    {
        $wrongCredentialsSet = [
            [
                'email'    => '',
                'password' => '',
            ],
            [
                'email'    => static::INVALID_EMAIL,
                'password' => static::FAKE_PASSWORD,
            ],
            [
                'email'    => '',
                'password' => static::PASSWORD,
            ],
            [
                'email'    => static::EMAIL,
                'password' => '',
            ],
        ];

        foreach ($wrongCredentialsSet as $credentials) {
            $responseValidation = $this->json(
                'POST',
                static::URL_LOGIN,
                $credentials
            );
            $responseValidation
                ->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY)
                ->assertJsonStructure(['errors', 'code']);
        }

        $responseFakeUser = $this->json(
            'POST',
            static::URL_LOGIN,
            [
                'email'    => static::FAKE_EMAIL,
                'password' => static::FAKE_PASSWORD,
            ]
        );
        $responseFakeUser
            ->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['message', 'code']);
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testLogoutPositive(): void
    {
        $response = $this->json(
            'POST',
            static::URL_LOGOUT,
            [],
            [
                'Authorization' => 'Bearer '.$this->token,
            ]
        );
        $response->assertStatus(HttpResponse::HTTP_NO_CONTENT);
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testLogoutNegative(): void
    {
        $response = $this->json('POST', static::URL_LOGOUT);
        $response
            ->assertStatus(HttpResponse::HTTP_UNAUTHORIZED)
            ->assertJsonStructure(['code', 'error']);
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testRegisterPositive(): void
    {
        $response = $this->json(
            'POST',
            static::URL_REGISTER,
            [
                'name'                  => static::NAME,
                'email'                 => static::NEW_USER_EMAIL,
                'password'              => static::PASSWORD,
                'password_confirmation' => static::PASSWORD,
                'terms_and_conditions'  => true,
            ]
        );
        $response->assertStatus(HttpResponse::HTTP_OK);

        $this->assertDatabaseHas('users', [
            'email' => static::NEW_USER_EMAIL,
        ]);

        Notification::assertSentTo(
            [User::query()->where('email', static::NEW_USER_EMAIL)->first()],
            VerifyEmailNotification::class
        );
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testRegisterTermsAndConditionsNegative(): void
    {
        $response = $this->json(
            'POST',
            static::URL_REGISTER,
            [
                'name'                  => static::NAME,
                'email'                 => static::NEW_USER_EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => static::NEW_PASSWORD,
                'terms_and_conditions'  => false,
            ]
        );
        $response
            ->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonStructure(['code', 'errors' => ['terms_and_conditions']]);
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testRegisterNegative(): void
    {
        $wrongCredentialsSet = [
            [
                'name'                  => '',
                'email'                 => '',
                'password'              => '',
                'password_confirmation' => '',
                'terms_and_conditions'  => false,
            ],
            [
                'name'                  => static::NAME,
                'email'                 => static::NEW_USER_EMAIL,
                'password'              => '',
                'password_confirmation' => '',
                'terms_and_conditions'  => false,
            ],
            [
                'name'                  => static::NAME,
                'email'                 => static::NEW_USER_EMAIL,
                'password'              => '',
                'password_confirmation' => '',
                'terms_and_conditions'  => false,
            ],
            [
                'name'                  => static::NAME,
                'email'                 => static::NEW_USER_EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => '',
                'terms_and_conditions'  => false,
            ],
            [
                'name'                  => static::NAME,
                'email'                 => static::NEW_USER_EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => static::NEW_PASSWORD,
                'terms_and_conditions'  => false,
            ],
            [
                'name'                  => static::NAME,
                'email'                 => static::EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => static::NEW_PASSWORD,
                'terms_and_conditions'  => true,
            ],
        ];

        foreach ($wrongCredentialsSet as $credentials) {
            $response = $this->json(
                'POST',
                static::URL_REGISTER,
                $credentials
            );
            $response
                ->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY)
                ->assertJsonStructure(['code', 'errors']);
        }
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testTokenRefreshPositive(): void
    {
        $response = $this->json(
            'GET',
            static::URL_TOKEN_REFRESH,
            [],
            [
                'Authorization' => 'Bearer '.$this->token,
            ]
        );
        $response
            ->assertStatus(HttpResponse::HTTP_OK)
            ->assertJsonStructure(['token', 'user', 'token_expires_at']);
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testTokenRefreshNegative(): void
    {
        $response = $this->json(
            'GET',
            static::URL_TOKEN_REFRESH
        );
        $response
            ->assertStatus(HttpResponse::HTTP_UNAUTHORIZED)
            ->assertJsonStructure(['code', 'error']);
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testResetPasswordEmailPositive(): void
    {
        $response = $this->json(
            'POST',
            static::URL_RESET_PASSWORD_EMAIL,
            [
                'email' => static::EMAIL,
            ]
        );
        $response
            ->assertStatus(HttpResponse::HTTP_OK)
            ->assertJsonStructure(['message']);

        $this->assertDatabaseHas('password_resets', [
            'email' => static::EMAIL,
        ]);

        Notification::assertSentTo(
            [$this->user],
            ResetPasswordNotification::class
        );
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testResetPasswordEmailNegative(): void
    {
        $emails = [
            '',
            static::FAKE_EMAIL,
            static::INVALID_EMAIL,
        ];

        foreach ($emails as $email) {
            $response = $this->json(
                'POST',
                static::URL_RESET_PASSWORD_EMAIL,
                [
                    'email' => $email,
                ]
            );
            $response
                ->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY)
                ->assertJsonStructure(['code', 'errors' => ['email']]);
        }
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testResetPasswordPositive(): void
    {
        $passwordResetToken = Password::broker()->createToken($this->user);
        $response = $this->json(
            'POST',
            static::URL_RESET_PASSWORD,
            [
                'email'                 => static::EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => static::NEW_PASSWORD,
                'token'                 => $passwordResetToken,
            ]
        );

        $response
            ->assertStatus(HttpResponse::HTTP_OK)
            ->assertJsonStructure(['message']);

        $this->assertDatabaseMissing('password_resets', [
            'email' => static::EMAIL,
        ]);

        // Login with new password.
        $this->assertTrue(
            Auth::attempt(['email' => static::EMAIL, 'password' => static::NEW_PASSWORD]) !== false
        );
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    public function testResetPasswordNegative(): void
    {

        $wrongCredentialsSet = [
            [
                'email'                 => '',
                'password'              => '',
                'password_confirmation' => '',
                'token'                 => '',
            ],
            [
                'email'                 => static::EMAIL,
                'password'              => '',
                'password_confirmation' => '',
                'token'                 => '',
            ],
            [
                'email'                 => static::EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => '',
                'token'                 => '',
            ],
            [
                'email'                 => static::EMAIL,
                'password'              => static::NEW_PASSWORD,
                'password_confirmation' => static::NEW_PASSWORD,
                'token'                 => '',
            ],
        ];

        foreach ($wrongCredentialsSet as $credentials) {
            $response = $this->json(
                'POST',
                static::URL_RESET_PASSWORD,
                $credentials
            );
            $response
                ->assertStatus(HttpResponse::HTTP_UNPROCESSABLE_ENTITY)
                ->assertJsonStructure(['code', 'errors']);
        }
    }
}
