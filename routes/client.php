<?php

declare(strict_types = 1);

use Illuminate\Support\Facades\Route;

Route::group(
    ['namespace' => 'Auth', 'as' => '.auth'],
    __DIR__.'/client/auth.php'
);

Route::group([
    'as'         => '.profile',
    'prefix'     => 'profile',
    'middleware' => ['jwt'],
], __DIR__.'/client/profile.php');

Route::group([
    'as'         => '.tour',
    'prefix'     => 'tour',
    'middleware' => ['jwt'],
], __DIR__.'/client/tour.php');

Route::group([
    'as'         => '.user',
    'prefix'     => 'user',
    'middleware' => ['jwt'],
], __DIR__.'/client/user.php');

Route::post('contact-us', [
    'as'   => '.contact-us',
    'uses' => 'GeneralController@contactUs',
]);
