<?php

declare(strict_types = 1);

Route::group(
    ['namespace' => 'Auth', 'as' => '.auth'],
    __DIR__.'/acp/auth.php'
);

Route::group([
    'middleware' => ['role:admin'],
], function () {
    /**
     * Dashboard.
     */
    Route::group(
        ['as' => '.dashboard'],
        __DIR__.'/acp/dashboard.php'
    );

    /**
     * User.
     */
    Route::group(
        ['as' => '.user', 'prefix' => 'user'],
        __DIR__.'/acp/user.php'
    );

    /**
     * Notification.
     */
    Route::group(
        [
            'namespace'  => '\App\Http\Controllers\General',
        ],
        __DIR__.'/general/notification.php'
    );
});
