<?php

declare(strict_types = 1);

Route::get('/', [
    'uses' => 'DashboardController@index',
]);
