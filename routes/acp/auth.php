<?php

declare(strict_types = 1);

Route::get('login', [
    'as'         => '.login',
    'uses'       => 'LoginController@showLoginForm',
    'middleware' => ['guest'],
]);

Route::post('login', [
    'as'         => '.login.check',
    'uses'       => 'LoginController@login',
    'middleware' => ['guest'],
]);

Route::post('logout', [
    'as'         => '.logout',
    'uses'       => 'LoginController@logout',
    'middleware' => ['role:admin'],
]);
