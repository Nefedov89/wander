<?php

declare(strict_types = 1);

use Illuminate\Support\Facades\Route;

Route::group(
    ['as' => '.notification', 'prefix' => 'notification'],
    function () {
        Route::get('unread', [
            'as'   => '.unread',
            'uses' => 'NotificationController@getUnreadNotifications',
        ]);

        Route::get('all', [
            'as'   => '.all',
            'uses' => 'NotificationController@getAll',
        ]);

        Route::patch('read/{databaseNotification}', [
            'as'   => '.read',
            'uses' => 'NotificationController@read',
        ]);

        Route::patch('read-all', [
            'as'   => '.read-all',
            'uses' => 'NotificationController@readAll',
        ]);
    }
);
