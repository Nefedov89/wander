<?php

declare(strict_types = 1);

Route::get('email/verify/{id}/{token}', [
    'as'   => 'auth.verification.verify',
    'uses' => 'Client\Auth\VerificationController@verify',
    'middleware' => ['web'],
]);

Route::group(
    [
        'namespace' => 'Client',
        'as'        => 'api',
        'prefix'     => 'api',
        'middleware' => ['api'],
    ],
    __DIR__.'/client.php'
);

Route::group(
    [
        'namespace'  => 'Acp',
        'as'         => 'acp',
        'prefix'     => 'acp',
        'middleware' => ['web'],
    ],
    __DIR__.'/acp.php'
);

Route::get('/{any}', function () {
    return view('index');
})->where('any', '.*');
