<?php

declare(strict_types = 1);

use Illuminate\Support\Facades\Route;

Route::post('/', [
    'as'   => '.add',
    'uses' => 'UserController@addUser',
]);

Route::patch('language', [
    'as'   => '.language',
    'uses' => 'UserController@changeLanguage',
]);
