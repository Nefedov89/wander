<?php

declare(strict_types = 1);

use Illuminate\Support\Facades\Route;

Route::get('/', [
    'as'   => '.index',
    'uses' => 'TourController@index',
]);

Route::post('/', [
    'as'   => '.store',
    'uses' => 'TourController@store',
]);

Route::get('{tour}', [
    'as'   => '.get',
    'uses' => 'TourController@get',
]);

Route::delete('{tour}', [
    'as'   => '.delete',
    'uses' => 'TourController@delete',
]);
