<?php

declare(strict_types = 1);

use Illuminate\Support\Facades\Route;

Route::post('login', [
    'as'   => '.login',
    'uses' => 'LoginController@login',
]);


Route::post('register', [
    'as'   => '.register',
    'uses' => 'RegisterController@register',
]);

Route::post('logout', [
    'as'         => '.logout',
    'uses'       => 'LoginController@logout',
    'middleware' => ['jwt'],
]);

Route::get('refresh-token', [
    'as'         => '.refresh-token',
    'uses'       => 'LoginController@refreshToken',
    'middleware' => ['jwt'],
]);

Route::post(
    'reset-password/email',
    [
        'as'   => '.reset-password.email',
        'uses' => 'ForgotPasswordController@sendResetLinkEmail',
    ]
);

Route::post(
    'reset-password',
    [
        'as'   => '.reset-password',
        'uses' => 'ResetPasswordController@reset',
    ]
);
