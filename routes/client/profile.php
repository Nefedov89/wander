<?php

declare(strict_types = 1);

use Illuminate\Support\Facades\Route;

Route::get('/', [
    'as'         => '.get',
    'uses'       => 'ProfileController@get',
]);

Route::patch('/', [
    'as'         => '.update',
    'uses'       => 'ProfileController@update',
]);
